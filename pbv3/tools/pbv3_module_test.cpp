#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "EndeavourCom.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"
#include "UIOCom.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string configfile = "config.json";
std::string runNumber = "0-0";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] datadir" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr << " -c, --config            Config for calibrating the AMAC. "
                 "Register settings are ignored. (default: "
              << configfile << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"config", required_argument, 0, 'c'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:c:r:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'c':
                configfile = optarg;
                break;
            case 'r':
                runNumber = optarg;
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Required paths missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    std::string outDir = argv[optind++];

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;
    logger(logDEBUG) << " outDir: " << outDir;

    // Output file
    std::string fileName =
        outDir + "/" +
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) +
        "_pbv3-module.json";
    logger(logINFO) << "Results stored in " << fileName;
    std::fstream outfile(fileName, std::ios::out);
    if (!outfile.is_open()) {
        logger(logERROR) << "Unable to create results file " << fileName;
        return 2;
    }

    //
    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Init com
    json config;
    if (!configfile.empty()) {
        std::ifstream fh_in(configfile);
        if (fh_in.is_open()) fh_in >> config;
    }

    logger(logINFO) << "Init AMAC";
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    amac->init();
    PBv3ConfigTools::saveConfigAMAC(amac, config);

    //
    // Start testing

    json testSum;
    testSum["program"] = argv[0];
    testSum["config"] = config;
    testSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    uint32_t test = 0;
    testSum["tests"][test++] =
        PBv3TestTools::measureHvCurrent(pbNum, tb, 10000);

    testSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    outfile << std::setw(4) << testSum << std::endl;

    outfile.close();

    return 0;
}
