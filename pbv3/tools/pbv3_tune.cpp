#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum;
std::string outfile = "config.json";
bool outappend = false;
std::string serial = "DUMMY";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] command [command...]"
              << std::endl;
    std::cerr << "List of possible COMMAND:" << std::endl;
    std::cerr << "  vddbg                            Tune the VDD bandgap."
              << std::endl;
    std::cerr << "  ambg                             Tune the AM bandgap."
              << std::endl;
    std::cerr << "  rampgain                         Tune the ADC slope."
              << std::endl;
    std::cerr << "  slope                            Recalibrate the ADC slope."
              << std::endl;
    std::cerr << "  ntc                              Recalibrate the NTCs."
              << std::endl;
    std::cerr << "  cur1v                            Tune the Cur1V CM block."
              << std::endl;
    std::cerr << "  cur10v                           Tune the Cur10V CM block."
              << std::endl;
    std::cerr << "  temp                             Calibrate the temperature "
                 "scale of PTAT/CTAT."
              << std::endl;
    std::cerr << "  all                              Tune everything."
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr << " -o, --output            File where to save the tuned "
                 "configuration. (default: stdout)"
              << std::endl;
    std::cerr << " -a, --append            Modify existing output file instead "
                 "of rewriting."
              << std::endl;
    std::cerr << " -s, --serial            Serial number of the powerboard. "
                 "(default: "
              << serial << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"output", required_argument, 0, 'o'},
            {"append", no_argument, 0, 'a'},
            {"serial", required_argument, 0, 's'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:o:as:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'o':
                outfile = optarg;
                break;
            case 'a':
                outappend = true;
                break;
            case 's':
                serial = optarg;
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    std::vector<std::string> commands;
    if (optind < argc) {
        commands.push_back(argv[optind++]);
    } else {
        std::cerr << "Required command argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    //
    // Open configuration
    json config;
    if (outappend) {
        std::ifstream fh_in(outfile);
        if (fh_in.is_open()) fh_in >> config;
    }
    PBv3ConfigTools::decorateConfig(config);
    config["component"] = serial;

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Init com
    logger(logINFO) << "Init AMAC";
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    amac->init();
    PBv3ConfigTools::configAMAC(amac, config, false);
    amac->initRegisters();

    //
    // Run over all commands
    for (const std::string &command : commands) {
        logger(logINFO) << "Running " << command;
        if (command == "vddbg")
            config.merge_patch(
                PBv3ConfigTools::tuneVDDBG(amac, tb->getCalDAC()));
        if (command == "ambg")
            config.merge_patch(
                PBv3ConfigTools::tuneAMBG(amac, tb->getCalDAC()));
        if (command == "rampgain")
            config.merge_patch(
                PBv3ConfigTools::tuneRampGain(amac, tb->getCalDAC()));
        if (command == "slope")
            config.merge_patch(
                PBv3ConfigTools::calibrateSlope(amac, tb->getCalDAC()));
        if (command == "offset")
            config.merge_patch(PBv3ConfigTools::calibrateOffset(amac));
        if (command == "ntc")
            config.merge_patch(PBv3ConfigTools::calibrateNTC(amac));
        if (command == "cur10v")
            config.merge_patch(PBv3ConfigTools::tuneCur10V(amac));
        if (command == "cur1v")
            config.merge_patch(PBv3ConfigTools::tuneCur1V(amac));
        if (command == "temp")
            config.merge_patch(PBv3ConfigTools::calibrateTemperature(amac));
        if (command == "all") {
            config.merge_patch(
                PBv3ConfigTools::calibrateSlope(amac, tb->getCalDAC()));
            config.merge_patch(PBv3ConfigTools::calibrateOffset(amac));
            config.merge_patch(
                PBv3ConfigTools::tuneVDDBG(amac, tb->getCalDAC()));
            config.merge_patch(
                PBv3ConfigTools::tuneAMBG(amac, tb->getCalDAC()));
            config.merge_patch(
                PBv3ConfigTools::tuneRampGain(amac, tb->getCalDAC()));
            config.merge_patch(PBv3ConfigTools::tuneCur10V(amac));
            config.merge_patch(PBv3ConfigTools::tuneCur1V(amac));
            config.merge_patch(PBv3ConfigTools::calibrateNTC(amac));
            config.merge_patch(PBv3ConfigTools::calibrateTemperature(amac));
        }
    }

    //
    // Save configuration
    std::ofstream fh_out(outfile);
    fh_out << std::setw(4) << config << std::endl;
    fh_out.close();

    return 0;
}
