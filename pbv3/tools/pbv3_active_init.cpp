#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBMassive.h"
#include "PBv3TestTools.h"

//------ SETTINGS
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    logger(logDEBUG) << "Settings";

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TBMassive> tb =
        std::dynamic_pointer_cast<PBv3TBMassive>(
            factory_pbv3tb.getPBv3TB("default"));
    if (tb == nullptr) {
        logger(logERROR) << "Error obtaining testbench object!";
        return 1;
    }

    for (uint32_t i = 0; i < 10; i++) tb->setCarrierOutputEnable(i, false);

    return 0;
}
