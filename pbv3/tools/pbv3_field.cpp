#include <Logger.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <cmath>
#include <iomanip>
#include <iostream>

#include "AMACv2.h"
#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0]
              << " [options] command [command parameters]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "commands" << std::endl;
    std::cerr << " list: list available fields" << std::endl;
    std::cerr << " read field: read current value of field" << std::endl;
    std::cerr << " write field value: write new value to field" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    // Determine the command
    if (argc <= optind) {
        usage(argv);
        return 1;
    }

    std::string command = argv[optind++];
    std::vector<std::string> params;
    while (optind < argc) {
        std::string p(argv[optind++]);
        params.push_back(p);
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    amac->init();

    // Now interpret command
    logger(logDEBUG) << "Sending commend to power-supply.";
    if (command == "list") {
        for (const AMACv2Field *field : amac->getFields())
            std::cout << field->getName() << std::endl;
    } else if (command == "read") {
        if (params.size() != 1) {
            logger(logERROR) << "Invalid number of parameters to read command.";
            logger(logERROR) << "";
            usage(argv);
            return 1;
        }
        AMACv2Field *field = amac->findField(params[0]);
        if (field == nullptr) {
            logger(logERROR) << "Invalid field name.";
            return 1;
        }

        uint32_t value = amac->rdField(params[0]);

        std::cout << "0x" << std::hex
                  << std::setw(std::ceil(field->getWidth() / 4.))
                  << std::setfill('0') << value << std::dec << std::endl;
    } else if (command == "write") {
        if (params.size() != 2) {
            logger(logERROR)
                << "Invalid number of parameters to write command.";
            logger(logERROR) << "";
            usage(argv);
            return 1;
        }
        AMACv2Field *field = amac->findField(params[0]);
        if (field == nullptr) {
            logger(logERROR) << "Invalid field name.";
            return 1;
        }

        uint32_t value = std::stoul(params[1], nullptr, 0);

        amac->wrField(params[0], value);
    } else {
        usage(argv);
    }

    return 0;
}
