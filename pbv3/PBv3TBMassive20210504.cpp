#include "PBv3TBMassive20210504.h"

// AMAC
#include "EndeavourComException.h"
#include "EndeavourRawUIO.h"
#include "EndeavourRawUIOMux.h"

// I2C
#include "PCA9548ACom.h"

// ADCs
#include "AD799X.h"
#include "MCP3425.h"

// IO expanders
#include "TCA9534.h"

// Register test bench
#include "PBv3TBRegistry.h"
REGISTER_PBV3TB(PBv3TBMassive20210504)

void PBv3TBMassive20210504::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "zturn_adapter") {
            m_zturn_adapter = kv.value();
        }
        if (kv.key() == "invertCMDin") {
            m_invertCMDin = int(kv.value());
        }
        if (kv.key() == "invertCMDout") {
            m_invertCMDout = int(kv.value());
        }
    }

    PBv3TBMassive::setConfiguration(config);
}

void PBv3TBMassive20210504::initDevices() {
    // Common
    PBv3TBMassive::initDevices();

    // Specific
    m_adc_pwr = std::make_shared<AD799X>(
        3.3, AD799X::AD7993,
        std::make_shared<PCA9548ACom>(0x23, 7, m_i2c_root));

    m_adc_common = std::make_shared<MCP3425>(
        std::make_shared<PCA9548ACom>(0x6A, 1, m_i2c_root));

    //
    // PBs
    m_pbsel = std::make_shared<TCA9534>(
        std::make_shared<PCA9548ACom>(0x38, 3, m_i2c_root));
    m_pbsel->setIO(0x0);

    std::shared_ptr<UIOCom> pbuio = std::make_shared<UIOCom>(m_pbdev, 0x10000);

    // Mapping for the CMDmux patch panel
    bool invertCMDin = m_zturn_adapter;
    bool invertCMDout = !m_zturn_adapter;

    if (m_invertCMDin > -1) invertCMDin = m_invertCMDin;
    if (m_invertCMDout > -1) invertCMDout = m_invertCMDout;

    m_pbs[0] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x0, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[1] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x8, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[2] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x1, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[3] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x9, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[4] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x2, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[5] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x3, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[6] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x4, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[7] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x5, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[8] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x6, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[9] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x7, invertCMDin, invertCMDout, m_pbsel, pbuio)));
}

void PBv3TBMassive20210504::powerTBOn() {
    uint32_t curr_val = m_pbsel->read();
    m_pbsel->write((curr_val & 0x0F) | 0x80);

    PBv3TBMassive::powerTBOn();
}

void PBv3TBMassive20210504::powerTBOff() {
    PBv3TBMassive::powerTBOff();

    uint32_t curr_val = m_pbsel->read();
    m_pbsel->write((curr_val & 0x0F) | 0x00);
}
