#include "PBv3TestTools.h"

#include "EndeavourComException.h"
#include "PBv3Utils.h"
#include "PowerSupplyChannel.h"
#ifdef FTDI
#include "EndeavourRawFTDI.h"
#endif

#include <chrono>
#include <memory>

namespace PBv3TestTools {
json testLvEnable(uint32_t pbNum, std::shared_ptr<PBv3TB> tb) {
    logger(logINFO) << "## Test LV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "LV_ENABLE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    uint32_t DCDCen_curr = amac->rdField(&AMACv2RegMap::DCDCen);

    //
    // Initialize

    // Set load to small value
    tb->loadOn(pbNum);
    logger(logINFO) << " -> Set load to 1.0A";
    tb->setLoad(pbNum, 1.0);

    // Disable DC/DC output
    logger(logINFO) << " --> Disabling DCDCen";
    amac->wrField(&AMACv2::DCDCen, 0);
    amac->wrField(&AMACv2::DCDCenC, 0);

    // Wait to stabilize
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    // read interesting values
    double lv_off = tb->getVout(pbNum);
    double Iin = tb->getVinCurrent();
    double Vin = tb->getVin();
    double iout = tb->getIload(pbNum);
    uint32_t Vdcdc = amac->readAM(AMACv2::AM::VDCDC);
    uint32_t VddLr = amac->readAM(AMACv2::AM::VDDLR);
    uint32_t DCDCin = amac->readAM(AMACv2::AM::DCDCIN);
    uint32_t NTC = amac->readAM(AMACv2::AM::NTCPB);
    uint32_t Cur10V = amac->readAM(AMACv2::AM::CUR10V);
    uint32_t Cur1V = amac->readAM(AMACv2::AM::CUR1V);
    uint32_t PTAT = amac->readAM(AMACv2::AM::PTAT);
    double efficiency = -1;
    double Iin_off = Iin;

    testSum["results"]["DCDCen"][0] = 0;
    testSum["results"]["VIN"][0] = Vin;
    testSum["results"]["IIN"][0] = Iin;
    testSum["results"]["VOUT"][0] = lv_off;
    testSum["results"]["IOUT"][0] = iout;
    testSum["results"]["AMACVDCDC"][0] = Vdcdc;
    testSum["results"]["AMACVDDLR"][0] = VddLr;
    testSum["results"]["AMACDCDCIN"][0] = DCDCin;
    testSum["results"]["AMACNTCPB"][0] = NTC;
    testSum["results"]["AMACCUR10V"][0] = Cur10V;
    testSum["results"]["AMACCUR1V"][0] = Cur1V;
    testSum["results"]["AMACPTAT"][0] = PTAT;
    testSum["results"]["EFFICIENCY"][0] = efficiency;

    bool pass_lv_off = (lv_off < 0.1);

    logger(logINFO) << " ---> VIN: " << Vin << " V";
    logger(logINFO) << " ---> IIN: " << Iin << " A";
    if (pass_lv_off)
        logger(logINFO) << " ---> VOUT: " << lv_off << " V";
    else
        logger(logERROR) << " ---> VOUT: " << lv_off << " V";

    //
    // Run test with output

    // Enable DC/DC
    logger(logINFO) << " --> Enabling DCDCen";
    amac->wrField(&AMACv2::DCDCen, 1);
    amac->wrField(&AMACv2::DCDCenC, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    double lv_on = tb->getVout(pbNum);
    Iin = tb->getVinCurrent();
    Vin = tb->getVin();
    iout = tb->getIload(pbNum);
    Vdcdc = amac->readAM(AMACv2::AM::VDCDC);
    VddLr = amac->readAM(AMACv2::AM::VDDLR);
    DCDCin = amac->readAM(AMACv2::AM::DCDCIN);
    NTC = amac->readAM(AMACv2::AM::NTCPB);
    Cur10V = amac->readAM(AMACv2::AM::CUR10V);
    Cur1V = amac->readAM(AMACv2::AM::CUR1V);
    PTAT = amac->readAM(AMACv2::AM::PTAT);
    efficiency = (lv_on * iout) / (Vin * (Iin - Iin_off));

    testSum["results"]["DCDCen"][1] = 1;
    testSum["results"]["VIN"][1] = Vin;
    testSum["results"]["IIN"][1] = Iin;
    testSum["results"]["VOUT"][1] = lv_on;
    testSum["results"]["IOUT"][1] = iout;
    testSum["results"]["AMACVDCDC"][1] = Vdcdc;
    testSum["results"]["AMACVDDLR"][1] = VddLr;
    testSum["results"]["AMACDCDCIN"][1] = DCDCin;
    testSum["results"]["AMACNTCPB"][1] = NTC;
    testSum["results"]["AMACCUR10V"][1] = Cur10V;
    testSum["results"]["AMACCUR1V"][1] = Cur1V;
    testSum["results"]["AMACPTAT"][1] = PTAT;
    testSum["results"]["EFFICIENCY"][1] = efficiency;

    bool pass_lv_on = (1.40 < lv_on && lv_on < 1.65);
    bool pass_eff = efficiency > 0.6;

    logger(logINFO) << " ---> VIN: " << Vin << " V";
    logger(logINFO) << " ---> IIN: " << Iin << " A";
    if (pass_lv_on)
        logger(logINFO) << " ---> VOUT: " << lv_on << " V";
    else
        logger(logERROR) << " ---> VOUT: " << lv_on << " V";

    if (pass_eff)
        logger(logINFO) << " ---> Effiency: " << efficiency;
    else
        logger(logERROR) << " ---> Effiency: " << efficiency;

    testSum["passed"] = true;
    if (!pass_lv_on || !pass_lv_off) {
        logger(logERROR) << " -> LV enable not working! " << lv_on << " "
                         << lv_off;
        testSum["passed"] = false;
    }

    if (!pass_eff) {
        logger(logERROR) << " -> Effiency too low! " << efficiency;
        testSum["passed"] = false;
    }

    if (testSum["passed"])
        logger(logINFO) << " LV_ENABLE passed! :)";
    else
        logger(logERROR) << " LV_ENABLE failed! :(";

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCen_curr);

    // Disable load
    tb->loadOff(pbNum);

    logger(logINFO) << "## End test LV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json testDCDCAdj(uint32_t pbNum, std::shared_ptr<PBv3TB> tb) {
    logger(logINFO) << "## Test DCDC_ADJUST ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "DCDC_ADJUST";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    uint32_t DCDCen_curr = amac->rdField(&AMACv2RegMap::DCDCen);
    uint32_t DCDCAdj_curr = amac->rdField(&AMACv2RegMap::DCDCAdj);
    // set small load
    tb->loadOn(pbNum);
    logger(logINFO) << " -> Set load to 0.2A";
    tb->setLoad(pbNum, 0.2);
    // enable DCDC w/ no offset
    logger(logINFO) << " --> Enabling DCDCen";
    amac->wrField(&AMACv2::DCDCen, 1);
    amac->wrField(&AMACv2::DCDCenC, 1);
    float shifts[4];
    std::cout << std::scientific << std::setprecision(3) << "DCDCAdj"
              << "\t"
              << "VOUT" << std::endl;
    for (int i = 0; i < 4; i++) {
        amac->wrField(&AMACv2::DCDCAdj, i);
        amac->wrField(&AMACv2::DCDCAdjC, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        shifts[i] = tb->getVout(pbNum);
        testSum["results"]["VOUT"][i] = shifts[i];
        testSum["results"]["DCDCAdj"][i] = i;
        std::cout << std::scientific << std::setprecision(3) << i << "\t"
                  << shifts[i] << std::endl;
    }
    float minusSix = (shifts[1] - shifts[0]) / shifts[0];
    float minusThirteen = (shifts[2] - shifts[0]) / shifts[0];
    float plusSix = (shifts[3] - shifts[0]) / shifts[0];

    bool minusSixPass = (-.0767 < minusSix && minusSix < -.0567);
    bool minusThirteenPass = (-.143 < minusThirteen && minusThirteen < -.123);
    bool plusSixPass = (.0567 < plusSix && plusSix < .0767);

    bool pass_dcdcadj = minusSixPass && minusThirteenPass && plusSixPass;

    if (minusThirteenPass) {
        logger(logINFO) << " ---> minusThirteen: " << minusThirteen;
    } else {
        logger(logERROR) << " ---> minusThirteen: " << minusThirteen;
    }

    if (minusSixPass) {
        logger(logINFO) << " ---> minusSix: " << minusSix;
    } else {
        logger(logERROR) << " ---> minusSix: " << minusSix;
    }

    if (plusSixPass) {
        logger(logINFO) << " ---> plusSix: " << plusSix;
    } else {
        logger(logERROR) << " ---> plusSix: " << plusSix;
    }

    testSum["passed"] = pass_dcdcadj;

    if (testSum["passed"]) {
        logger(logINFO) << " DCDCAdj passed! :)";
    } else {
        logger(logERROR) << " DCDCAdj failed! :(";
    }
    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCAdj, DCDCAdj_curr);
    amac->wrField(&AMACv2RegMap::DCDCAdjC, DCDCAdj_curr);

    // Disable load
    tb->loadOff(pbNum);
    logger(logINFO) << "## End test DCDC_ADJUST ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    return testSum;
}

// Range is iout in A
json measureEfficiency(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, double step,
                       double min, double max, double VinSet, uint32_t trails) {
    logger(logINFO) << "## Measuring DCDC efficiency ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "DCDCEFFICIENCY";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    // Store current state
    uint32_t DCDCen_curr = amac->rdField(&AMACv2RegMap::DCDCen);
    uint32_t DCDCenC_curr = amac->rdField(&AMACv2RegMap::DCDCenC);

    //
    // Initialize

    // Set load to 0
    tb->loadOn(pbNum);
    tb->setLoad(pbNum, 0);

    logger(logINFO) << " --> Vin = " << VinSet << "V";
    tb->setVin(VinSet);

    logger(logINFO) << " --> Turn off DCDC ..";
    amac->wrField(&AMACv2RegMap::DCDCen, 0);
    amac->wrField(&AMACv2RegMap::DCDCenC, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    double Iin_offset = tb->getVinCurrent();
    logger(logINFO) << " --> Get baseline current: (" << Iin_offset << ")A";
    testSum["results"]["IINOFFSET"] = Iin_offset;

    logger(logINFO) << " --> Turn on DCDC ...";
    amac->wrField(&AMACv2RegMap::DCDCen, 1);
    amac->wrField(&AMACv2RegMap::DCDCenC, 1);

    // unsigned dwell_time = .1; //s
    unsigned dwell_time_s = 2;  // s
    unsigned dwell_time = 0.1;  // s
    testSum["properties"]["DWELLTIMELONG"] = dwell_time;
    testSum["properties"]["DWELLTIMESHORT"] = dwell_time_s;

    logger(logINFO) << " --> Starting measurement ...";
    std::cout << "VIN"
              << "\t"
              << "IIN"
              << "\t"
              << "VOUT"
              << "\t"
              << "IOUT"
              << "\t"
              << "IOUTSET"
              << "\t"
              << "AMACVDCDC"
              << "\t"
              << "AMACVDDLR"
              << "\t"
              << "AMACDCDCIN"
              << "\t"
              << "AMACNTCPB"
              << "\t"
              << "AMACCUR10V"
              << "\t"
              << "AMACCUR1V"
              << "\t"
              << "AMACPTAT"
              << "\t"
              << "EFFICIENCY" << std::endl;

    // Loop over currents
    int index = 0;

    for (double iout = min; iout <= max; iout += step) {
        // allowing system to reach thermal equilibrium
        std::this_thread::sleep_for(std::chrono::seconds(dwell_time));
        logger(logDEBUG) << " --> Setting " << iout << "mA load!";
        // Set Current
        tb->setLoad(pbNum, iout);

        // Wait for temp and everything to settle
        std::this_thread::sleep_for(std::chrono::seconds(dwell_time_s));

        // Read AMAC values
        uint32_t Vdcdc, VddLr, DCDCin, NTC, Cur10V, Cur1V, PTAT;

        for (uint32_t trail = 0; trail < trails; trail++) {
            Vdcdc = amac->readAM(AMACv2::AM::VDCDC);
            VddLr = amac->readAM(AMACv2::AM::VDDLR);
            DCDCin = amac->readAM(AMACv2::AM::DCDCIN);
            NTC = amac->readAM(AMACv2::AM::NTCPB);
            Cur10V = amac->readAM(AMACv2::AM::CUR10V);
            Cur1V = amac->readAM(AMACv2::AM::CUR1V);
            PTAT = amac->readAM(AMACv2::AM::PTAT);

            double Vin = tb->getVin();
            double Iin = tb->getVinCurrent();
            double Iout = tb->getIload(pbNum);

            double Vout = tb->getVout(pbNum);

            double efficiency = (Vout * Iout) / (Vin * (Iin - Iin_offset));
            std::cout << Vin << "\t" << Iin << "\t" << Vout << "\t" << Iout
                      << "\t" << iout << "\t" << Vdcdc << "\t" << VddLr << "\t"
                      << DCDCin << "\t" << NTC << "\t" << Cur10V << "\t"
                      << Cur1V << "\t" << PTAT << "\t" << efficiency
                      << std::endl;
            testSum["results"]["VIN"][index] = Vin;
            testSum["results"]["IIN"][index] = Iin;
            testSum["results"]["VOUT"][index] = Vout;
            testSum["results"]["IOUT"][index] = Iout;
            testSum["results"]["IOUTSET"][index] = iout;
            testSum["results"]["AMACVDCDC"][index] = Vdcdc;
            testSum["results"]["AMACVDDLR"][index] = VddLr;
            testSum["results"]["AMACDCDCIN"][index] = DCDCin;
            testSum["results"]["AMACNTCPB"][index] = NTC;
            testSum["results"]["AMACCUR10V"][index] = Cur10V;
            testSum["results"]["AMACCUR1V"][index] = Cur1V;
            testSum["results"]["AMACPTAT"][index] = PTAT;
            testSum["results"]["EFFICIENCY"][index] = efficiency;

            index++;
        }
    }

    logger(logINFO) << " --> Done!! Turning off load!";

    // Disable load
    tb->loadOff(pbNum);

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCenC_curr);

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

json testHvEnable(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                  unsigned frequency) {
    logger(logINFO) << "## Test HV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "HV_ENABLE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    std::shared_ptr<PowerSupplyChannel> hv = tb->getHVPS();

    uint32_t CntSetHV0frq_curr = amac->rdField(&AMACv2RegMap::CntSetHV0frq);
    uint32_t CntSetCHV0frq_curr = amac->rdField(&AMACv2RegMap::CntSetCHV0frq);
    uint32_t CntSetHV0en_curr = amac->rdField(&AMACv2RegMap::CntSetHV0en);
    uint32_t CntSetCHV0en_curr = amac->rdField(&AMACv2RegMap::CntSetCHV0en);
    uint32_t HVcurGain_curr = amac->rdField(&AMACv2RegMap::HVcurGain);

    logger(logINFO) << " ---> HVcurGain: " << 4;
    amac->wrField(&AMACv2RegMap::HVcurGain, 4);

    logger(logINFO) << " ---> CntSetHV0frq: " << frequency
                    << " # 0=high, 1=25kHz, 2=50kHz, 3=100kHz";
    amac->wrField(&AMACv2::CntSetHV0frq, frequency);
    amac->wrField(&AMACv2::CntSetCHV0frq, frequency);

    // Configure PS
    logger(logINFO) << " -> Turning everything off.";
    hv->turnOff();
    hv->setVoltageLevel(0);
    hv->setCurrentProtect(1e-3);

    // See what we see
    logger(logINFO) << " -> Turning on HVPS";
    hv->turnOn();
    hv->rampVoltageLevel(-500, 40);
    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Turn off HVmux
    logger(logINFO) << " --> Disabling CntSetHV0en";
    amac->wrField(&AMACv2::CntSetHV0en, 0);
    amac->wrField(&AMACv2::CntSetCHV0en, 0);
    std::this_thread::sleep_for(std::chrono::seconds(2));  // wait to stabilize

    //
    // Measure OFF state
    double hv_v_off = fabs(hv->measureVoltage());
    double hv_i_off = fabs(hv->measureCurrent());
    double hvout_i_off = tb->getHVoutCurrent(pbNum);
    uint32_t HVret_off = amac->readAM(AMACv2::AM::HVRET);

    testSum["results"]["CntSetHV0frq"][0] = frequency;
    testSum["results"]["CntSetHV0en"][0] = 0;
    testSum["results"]["HVcurGain"][0] = 4;
    testSum["results"]["HVVIN"][0] = hv_v_off;
    testSum["results"]["HVIIN"][0] = hv_i_off;
    testSum["results"]["HVIOUT"][0] = hvout_i_off;
    testSum["results"]["AMACHVRET"][0] = HVret_off;

    // Check status
    bool pass_hv_v_off = hv_v_off > 450;
    bool pass_HVret_off = HVret_off < 200;
    bool pass_hvout_i_off = hvout_i_off < 2e-6;

    if (pass_hv_v_off)
        logger(logINFO) << " ---> HVVIN: " << hv_v_off << " V";
    else
        logger(logERROR) << " ---> HVVIN: " << hv_v_off << " V";
    logger(logINFO) << " ---> HVIIN: " << hv_i_off << " A";
    if (pass_hvout_i_off)
        logger(logINFO) << " ---> HVIOUT: " << hvout_i_off << " A";
    else
        logger(logERROR) << " ---> HVIOUT: " << hvout_i_off << " A";
    if (pass_HVret_off)
        logger(logINFO) << " ---> AMACHVRET: " << HVret_off;
    else
        logger(logERROR) << " ---> AMACHVRET: " << HVret_off;

    //
    // Measure ON state
    logger(logINFO) << " --> Enabling CntSetHV0en";
    amac->wrField(&AMACv2::CntSetHV0en, 1);
    amac->wrField(&AMACv2::CntSetCHV0en, 1);
    std::this_thread::sleep_for(std::chrono::seconds(2));  // wait to stabilize

    std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    double hv_v_on = fabs(hv->measureVoltage());
    double hv_i_on = fabs(hv->measureCurrent());
    double hvout_i_on = tb->getHVoutCurrent(pbNum);
    uint32_t HVret_on = amac->readAM(AMACv2::AM::HVRET);

    testSum["results"]["CntSetHV0frq"][1] = frequency;
    testSum["results"]["CntSetHV0en"][1] = 1;
    testSum["results"]["HVcurGain"][1] = 4;
    testSum["results"]["HVVIN"][1] = hv_v_on;
    testSum["results"]["HVIIN"][1] = hv_i_on;
    testSum["results"]["HVIOUT"][1] = hvout_i_on;
    testSum["results"]["AMACHVRET"][1] = HVret_on;

    // Check status
    bool pass_hv_v_on = hv_v_on > 450;
    bool pass_HVret_on = HVret_on > 300;
    bool pass_hv_i = (hv_i_on - hv_i_off) > 0.8e-3;
    bool pass_hvout_i_on = hvout_i_on > 0.8e-3;

    if (pass_hv_v_on)
        logger(logINFO) << " ---> HVVIN: " << hv_v_on << " V";
    else
        logger(logERROR) << " ---> HVVIN: " << hv_v_on << " V";
    if (pass_hv_i)
        logger(logINFO) << " ---> HVIIN: " << hv_i_on << " A";
    else
        logger(logERROR) << " ---> HVIIN: " << hv_i_on << " A";
    if (pass_hvout_i_on)
        logger(logINFO) << " ---> HVIOUT: " << hvout_i_on << " A";
    else
        logger(logERROR) << " ---> HVIOUT: " << hvout_i_on << " A";
    if (pass_HVret_on)
        logger(logINFO) << " ---> AMACHVRET: " << HVret_on;
    else
        logger(logERROR) << " ---> AMACHVRET: " << HVret_on;

    // Done...
    logger(logINFO) << " -> Turning off HVPS";
    hv->rampVoltageLevel(0, 40);
    hv->turnOff();

    // Error checking
    testSum["passed"] = true;

    if (!pass_hv_v_on || !pass_hv_v_off) {
        logger(logERROR)
            << " -> HV power supply cannot reach nominal voltage...";
        testSum["passed"] = false;
    }

    if (!pass_hv_i) {
        logger(logERROR) << " -> HV current does not change on toggle! (ΔI = "
                         << hv_i_on - hv_i_off << " A)";
        testSum["passed"] = false;
    }

    if (!pass_hvout_i_on || !pass_hvout_i_off) {
        logger(logERROR) << " -> HV sensor current is wrong!";
        testSum["passed"] = false;
    }

    if (!pass_HVret_on || !pass_HVret_off) {
        logger(logERROR) << " -> AMAC does not see a change in HV current!";
        testSum["passed"] = false;
    }

    if (testSum["passed"])
        logger(logINFO) << " HV_ENABLE passed! :)";
    else
        logger(logERROR) << " HV_ENABLE failed! :(";
    logger(logINFO) << "## End test HV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::CntSetHV0frq, CntSetHV0frq_curr);
    amac->wrField(&AMACv2RegMap::CntSetCHV0frq, CntSetCHV0frq_curr);
    amac->wrField(&AMACv2RegMap::CntSetHV0en, CntSetHV0en_curr);
    amac->wrField(&AMACv2RegMap::CntSetCHV0en, CntSetCHV0en_curr);
    amac->wrField(&AMACv2RegMap::HVcurGain, HVcurGain_curr);

    return testSum;
}

json readStatus(std::shared_ptr<AMACv2> amac,
                std::shared_ptr<PowerSupplyChannel> lv,
                std::shared_ptr<Bk85xx> load,
                std::shared_ptr<PowerSupplyChannel> hv, uint32_t tests) {
    logger(logINFO) << "## Reading current status  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "STATUS";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    for (uint32_t index = 0; index < tests; index++) {
        // ADCs
        uint32_t Vdcdc = amac->readAM(AMACv2::AM::VDCDC);
        uint32_t VddLr = amac->readAM(AMACv2::AM::VDDLR);
        uint32_t VddLrLo = amac->readAM(AMACv2::AM::VDDLRLo);
        uint32_t VddLrHi = amac->readAM(AMACv2::AM::VDDLRHi);
        uint32_t DCDCin = amac->readAM(AMACv2::AM::DCDCIN);
        uint32_t VDDREG = amac->readAM(AMACv2::AM::VDDREG);
        uint32_t AM900BG = amac->readAM(AMACv2::AM::AM900BG);
        uint32_t AM600BG = amac->readAM(AMACv2::AM::AM600BG);
        uint32_t CALin = amac->readAM(AMACv2::AM::CAL);
        uint32_t CTAT = amac->readAM(AMACv2::AM::CTAT);
        uint32_t NTCx = amac->readAM(AMACv2::AM::NTCX);
        uint32_t NTCy = amac->readAM(AMACv2::AM::NTCY);
        uint32_t NTCpb = amac->readAM(AMACv2::AM::NTCPB);
        uint32_t Cur10V = amac->readAM(AMACv2::AM::CUR10V);
        uint32_t Cur1V = amac->readAM(AMACv2::AM::CUR1V);
        uint32_t HVret = amac->readAM(AMACv2::AM::HVRET);
        uint32_t PTAT = amac->readAM(AMACv2::AM::PTAT);

        double Vin = (lv != nullptr) ? lv->measureVoltage() : 0.;
        double Iin = (lv != nullptr) ? lv->measureCurrent() : 0.;

        double Vout = (load != nullptr) ? load->getValues().vol * 1e-3 : 0.;
        double Iout = (load != nullptr) ? load->getValues().cur * 1e-3 : 0.;

        double HV_Vin = (hv != nullptr) ? hv->measureVoltage() : 0.;
        double HV_Iin = (hv != nullptr) ? hv->measureCurrent() : 0.;

        double ADC0 = 0.;
        double ADC1 = 0.;
        double ADC2 = 0.;
        double ADC3 = 0.;
#ifdef FTDI
        EndeavourRawFTDI *ftdiobj =
            dynamic_cast<EndeavourRawFTDI *>(amac->raw().get());
        if (ftdiobj != nullptr) {
            ADC0 = ftdiobj->getADC()->read(0);
            ADC1 = ftdiobj->getADC()->read(1);
            ADC2 = ftdiobj->getADC()->read(2);
            ADC3 = ftdiobj->getADC()->read(3);
        }
#endif
        std::cout << "Vdcdc\tVddLr\tDCDCin\tVddReg\t"
                  << "Am900Bg\tAm600Bg\tCal"
                  << "\tCTAT\tNTCx\tNTCy\tNTCpb"
                  << "\tCur10V\tCur1V\tVddLrLo\tVddLrHi"
                  << "\tHVret\tPTAT\tVin\tIin\tVout\tIout"
                  << "\tHV_Vin\tHV_Iin\tADC0\tADC1\tADC2\tADC3" << std::endl;
        std::cout << Vdcdc << "\t" << VddLr << "\t" << DCDCin << "\t" << VDDREG
                  << "\t" << AM900BG << "\t" << AM600BG << "\t" << CALin << "\t"
                  << CTAT << "\t" << NTCx << "\t" << NTCy << "\t" << NTCpb
                  << "\t" << Cur10V << "\t" << Cur1V << "\t" << VddLrLo << "\t"
                  << VddLrHi << "\t" << HVret << "\t" << PTAT << "\t" << Vin
                  << "\t" << Iin << "\t" << Vout << "\t" << Iout << "\t"
                  << HV_Vin << "\t" << HV_Iin << "\t" << ADC0 << "\t" << ADC1
                  << "\t" << ADC2 << "\t" << ADC3 << std::endl;

        testSum["results"]["AMACVDCDC"][index] = Vdcdc;
        testSum["results"]["AMACVDDLR"][index] = VddLr;
        testSum["results"]["AMACVDDLRLO"][index] = VddLrLo;
        testSum["results"]["AMACVDDLRHI"][index] = VddLrHi;
        testSum["results"]["AMACDCDCIN"][index] = DCDCin;
        testSum["results"]["AMACVDDREG"][index] = VDDREG;
        testSum["results"]["AMACAM900BG"][index] = AM900BG;
        testSum["results"]["AMACAM600BG"][index] = AM600BG;
        testSum["results"]["AMACCAL"][index] = CALin;
        testSum["results"]["AMACCTAT"][index] = CTAT;
        testSum["results"]["AMACNTCX"][index] = NTCx;
        testSum["results"]["AMACNTCY"][index] = NTCy;
        testSum["results"]["AMACNTCPB"][index] = NTCpb;
        testSum["results"]["AMACCUR10V"][index] = Cur10V;
        testSum["results"]["AMACCUR1V"][index] = Cur1V;
        testSum["results"]["AMACHVRET"][index] = HVret;
        testSum["results"]["AMACPTAT"][index] = PTAT;
        testSum["results"]["VIN"][index] = Vin;
        testSum["results"]["IIN"][index] = Iin;
        testSum["results"]["VOUT"][index] = Vout * 1e-3;
        testSum["results"]["IOUT"][index] = Iout * 1e-3;
        testSum["results"]["HVVIN"][index] = HV_Vin;
        testSum["results"]["HVIIN"][index] = HV_Iin;
        testSum["results"]["ADC0"][index] = ADC0;
        testSum["results"]["ADC1"][index] = ADC1;
        testSum["results"]["ADC2"][index] = ADC2;
        testSum["results"]["ADC3"][index] = ADC3;
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

json runBER(std::shared_ptr<AMACv2> amac, uint32_t trails) {
    logger(logINFO) << "## Test BER ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "BER";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    testSum["properties"]["TRAILS"] = trails;

    // Run the test
    uint good = 0;
    for (uint i = 0; i < trails; i++) {
        try {
            uint valin = rand() * 0xFFFFFFFF;
            amac->write_reg(120, valin);
            uint valout = amac->read_reg(120);

            if (valin == valout)
                good++;
            else
                logger(logERROR) << " -> Write: 0x" << std::hex << valin
                                 << ", Read: " << valout << std::dec;
        } catch (EndeavourComException &e) {
            logger(logDEBUG) << e.what();
        }
    }
    float reliability = ((float)good) / trails;

    // Store the results
    testSum["results"]["RELIABILITY"] = reliability;

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = reliability == 1;

    if (testSum["passed"]) {
        logger(logINFO) << " --> RELIABILITY: " << reliability;
    } else {
        logger(logERROR) << " --> RELIABILITY: " << reliability;
    }

    if (testSum["passed"])
        logger(logINFO) << " BER passed! :)";
    else
        logger(logERROR) << " BER failed! :(";

    logger(logINFO) << "## End test BER ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json calibrateAMACoffset(std::shared_ptr<AMACv2> amac, bool scanSettings) {
    logger(logINFO) << "## Calibrating AMAC offset  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "AMOFFSET";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    int counts;

    amac->wrField(&AMACv2RegMap::AMzeroCalib, 1);
    amac->wrField(&AMACv2RegMap::AMzeroCalibC, 1);

    uint32_t gain_curr = amac->rdField(&AMACv2RegMap::AMintCalib);

    int index = 0;
    for (uint32_t gain_set = ((scanSettings) ? 0 : gain_curr);
         gain_set < ((scanSettings) ? 16 : (gain_curr + 1)); gain_set++) {
        amac->wrField(&AMACv2RegMap::AMintCalib, gain_set);
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

        counts = amac->rdField(&AMACv2RegMap::Ch0Value);
        testSum["results"]["CH0"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch1Value);
        testSum["results"]["CH1"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch2Value);
        testSum["results"]["CH2"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch3Value);
        testSum["results"]["CH3"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch4Value);
        testSum["results"]["CH4"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch5Value);
        testSum["results"]["CH5"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch6Value);
        testSum["results"]["CH6"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch7Value);
        testSum["results"]["CH7"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch8Value);
        testSum["results"]["CH8"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch9Value);
        testSum["results"]["CH9"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch10Value);
        testSum["results"]["CH10"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch11Value);
        testSum["results"]["CH11"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch12Value);
        testSum["results"]["CH12"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch13Value);
        testSum["results"]["CH13"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch14Value);
        testSum["results"]["CH14"][index] = counts;
        counts = amac->rdField(&AMACv2RegMap::Ch15Value);
        testSum["results"]["CH15"][index] = counts;

        testSum["results"]["AMintCalib"][index] = gain_set;

        index++;
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    amac->initRegisters();

    return testSum;
}

json calibrateAMACslope(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, double step,
                        bool scanSettings) {
    // Get useful tools from the TB
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    std::shared_ptr<DACDevice> CALdac = tb->getCalDAC();

    // Start ramping
    logger(logINFO) << "## Calibrating AMAC slope  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "AMSLOPE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::cout << "CALIN"
              << "\t"
              << "AMbg"
              << "\t"
              << "AMintCalib"
              << "\t"
              << "AMACCAL" << std::endl;

    uint32_t gain_curr = amac->rdField(&AMACv2RegMap::AMintCalib);
    uint32_t bg_curr = amac->rdField(&AMACv2RegMap::AMbg);

    // Run the test
    uint32_t index = 0;
    double CALact = 0;
    uint32_t CALamac = 0;
    for (uint32_t bg_set = ((scanSettings) ? 0 : bg_curr);
         bg_set < ((scanSettings) ? 16 : (bg_curr + 1)); bg_set++) {
        amac->wrField(&AMACv2RegMap::AMbg, bg_set);

        for (uint32_t gain_set = (scanSettings) ? 0 : gain_curr;
             gain_set < ((scanSettings) ? 16 : (gain_curr + 1)); gain_set++) {
            amac->wrField(&AMACv2RegMap::AMintCalib, gain_set);

            // Longer wait for the first value due to RC constant
            CALdac->set(0);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            for (double CALin = 0; CALin < 1.01; CALin += step) {
                // set step
                CALact = CALdac->set(CALin);
                // wait
                std::this_thread::sleep_for(std::chrono::milliseconds(5));
                // measure
                CALamac = amac->readAM(AMACv2::AM::CAL);

                testSum["results"]["CALIN"][index] = CALact;
                testSum["results"]["AMACCAL"][index] = CALamac;
                testSum["results"]["AMintCalib"][index] = gain_set;
                testSum["results"]["AMbg"][index] = bg_set;
                index++;
                std::cout << CALact << "\t" << bg_set << "\t" << gain_set
                          << "\t" << CALamac << std::endl;
            }  // end cycling through voltages
        }      // end cycling through gain
    }          // end cycling through bandgap
    CALdac->set(0);

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::AMintCalib, gain_curr);
    amac->wrField(&AMACv2RegMap::AMbg, bg_curr);

    return testSum;
}

json testOF(uint32_t pbNum, std::shared_ptr<PBv3TB> tb) {
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    logger(logINFO) << "## Test OF ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "OF";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    logger(logINFO) << " --> Enabling OF";
    tb->setOFin(pbNum, true);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    double Iin = tb->getVinCurrent();
    double Vin = tb->getVin();
#ifdef FTDI
    double ADC0 =
        dynamic_cast<EndeavourRawFTDI *>(amac->raw().get())->getADC()->read(0);
#else
    double ADC0 =
        tb->readCarrierOutput(pbNum, PBv3TB::CARRIER_OUTPUT::LINPOL1V4);
#endif  // FTDI
    testSum["results"]["OF"][1] = 1;
    testSum["results"]["VIN"][1] = Vin;
    testSum["results"]["IIN"][1] = Iin;
    testSum["results"]["linPOLV"][1] = ADC0;
    bool OFon_success = ADC0 < 0.25;
    logger(logINFO) << " ---> VIN: " << Vin << " V";
    logger(logINFO) << " ---> IIN: " << Iin << " A";
    if (OFon_success)
        logger(logINFO) << " ---> linPOLV:  " << ADC0 << " V";
    else
        logger(logERROR) << " ---> linPOLV:  " << ADC0 << " V";

    logger(logINFO) << " --> Disabling OF";
    tb->setOFin(pbNum, false);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    Iin = tb->getVinCurrent();
    Vin = tb->getVin();
#ifdef FTDI
    ADC0 =
        dynamic_cast<EndeavourRawFTDI *>(amac->raw().get())->getADC()->read(0);
#else
    ADC0 = tb->readCarrierOutput(pbNum, PBv3TB::CARRIER_OUTPUT::LINPOL1V4);
#endif  // FTDI
    testSum["results"]["OF"][0] = 0;
    testSum["results"]["VIN"][0] = Vin;
    testSum["results"]["IIN"][0] = Iin;
    testSum["results"]["linPOLV"][0] = ADC0;
    bool OFoff_success = ADC0 > 1.3;
    logger(logINFO) << " ---> VIN: " << Vin << " V";
    logger(logINFO) << " ---> IIN: " << Iin << " A";
    if (OFoff_success)
        logger(logINFO) << " ---> linPOLV:  " << ADC0 << " V";
    else
        logger(logERROR) << " ---> linPOLV:  " << ADC0 << " V";

    // Final
    testSum["passed"] = OFon_success && OFoff_success;

    if (testSum["passed"])
        logger(logINFO) << " OF passed! :)";
    else
        logger(logERROR) << " OF failed! :(";

    logger(logINFO) << "## End test OF ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = OFon_success && OFoff_success;

    return testSum;
}

json measureHvSense(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                    uint32_t trails) {
    logger(logINFO) << "## Measure HV sense  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "HVSENSE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    std::shared_ptr<PowerSupplyChannel> hv = tb->getHVPS();

    uint32_t CntSetHV0en_curr = amac->rdField(&AMACv2RegMap::CntSetHV0en);
    uint32_t CntSetCHV0en_curr = amac->rdField(&AMACv2RegMap::CntSetCHV0en);
    uint32_t HVcurGain_curr = amac->rdField(&AMACv2RegMap::HVcurGain);

    // Define the requested current measurement
    double ileak_min = 0;
    double ileak_max = 1.0e-3;
    double ileak_step = 5e-7;  // step in decade of ileak
    double resist = 500e3;     // 500 kOhm resistor in HV circui

    logger(logINFO) << " --> Turn on PS";
    hv->turnOff();
    hv->setVoltageLevel(-ileak_min * resist);
    hv->setCurrentProtect(1e-3);

    hv->turnOn();

    logger(logINFO) << " --> Turn on HVmux";
    amac->wrField(&AMACv2RegMap::CntSetHV0en, 1);
    amac->wrField(&AMACv2RegMap::CntSetCHV0en, 1);

    logger(logINFO) << " --> Starting measurement";
    std::cout << "HVVSET"
              << "\t"
              << "HVV"
              << "\t"
              << "HVI"
              << "\t"
              << "HVIOUT"
              << "\t"
              << "AMACGAIN0"
              << "\t"
              << "AMACGAIN1"
              << "\t"
              << "AMACGAIN2"
              << "\t"
              << "AMACGAIN4"
              << "\t"
              << "AMACGAIN8" << std::endl;

    int counter = 0;
    int index = 0;
    int32_t step_decade =
        std::floor(std::log10(ileak_step));  // decade of the current step
    uint32_t steps_in_decade =
        std::floor(std::pow(10, step_decade + 1) / ileak_step);

    for (double ileak = ileak_min; ileak <= ileak_max; ileak += ileak_step) {
        double hvset = ileak * resist;
        hv->setVoltageLevel(-hvset);
        std::this_thread::sleep_for(
            std::chrono::seconds(2));  // wait to stabilize

        for (uint32_t i = 0; i < trails; i++) {
            double hv_v = fabs(hv->measureVoltage());
            double hv_i = fabs(hv->measureCurrent());
            double hvout_i = tb->getHVoutCurrent(pbNum);

            unsigned val[5];
            for (unsigned g = 0; g < 5; g++) {
                if (g == 0)
                    amac->wrField(&AMACv2RegMap::HVcurGain, 0);
                else
                    amac->wrField(&AMACv2RegMap::HVcurGain, pow(2, g - 1));

                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                val[g] = amac->readAM(AMACv2::AM::HVRET);
            }
            std::cout << std::scientific << std::setprecision(3) << hvset
                      << "\t" << hv_v << "\t" << hv_i << "\t" << hvout_i << "\t"
                      << std::dec << val[0] << "\t" << val[1] << "\t" << val[2]
                      << "\t" << val[3] << "\t" << val[4] << std::endl;
            testSum["results"]["HVVSET"][index] = hvset;
            testSum["results"]["HVV"][index] = hv_v;
            testSum["results"]["HVI"][index] = hv_i;
            testSum["results"]["HVIOUT"][index] = hvout_i;
            testSum["results"]["AMACGAIN0"][index] = val[0];
            testSum["results"]["AMACGAIN1"][index] = val[1];
            testSum["results"]["AMACGAIN2"][index] = val[2];
            testSum["results"]["AMACGAIN4"][index] = val[3];
            testSum["results"]["AMACGAIN8"][index] = val[4];
            index++;
        }

        // Increment counter
        counter++;
        if ((counter % steps_in_decade == 0) && counter != 0) {
            ileak_step = ileak_step * 10;
            ileak = 0;
        }
    }

    hv->rampVoltageLevel(0, 40);
    hv->turnOff();

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::CntSetHV0en, CntSetHV0en_curr);
    amac->wrField(&AMACv2RegMap::CntSetCHV0en, CntSetCHV0en_curr);
    amac->wrField(&AMACv2RegMap::HVcurGain, HVcurGain_curr);

    return testSum;
}

json measureHvCurrent(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                      uint32_t trails) {
    logger(logINFO) << "## Measure HV current  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "HVCURRENT";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    std::shared_ptr<PowerSupplyChannel> hv = tb->getHVPS();

    logger(logINFO) << " --> Starting measurement";
    std::cout << "HV_v\t\tHV_i\t\tGain 0\tGain 1\tGain 2\tGain 4\tGain 8"
              << std::endl;

    for (uint32_t index = 0; index < trails; index++) {
        double hv_v = hv->measureVoltage();
        double hv_i = hv->measureCurrent();

        unsigned val[5];
        for (unsigned g = 0; g < 5; g++) {
            if (g == 0)
                amac->wrField(&AMACv2RegMap::HVcurGain, 0);
            else
                amac->wrField(&AMACv2RegMap::HVcurGain, pow(2, g - 1));

            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            val[g] = amac->readAM(AMACv2::AM::HVRET);
        }
        std::cout << std::scientific << std::setprecision(3) << hv_v << "\t"
                  << hv_i << "\t" << std::dec << val[0] << "\t" << val[1]
                  << "\t" << val[2] << "\t" << val[3] << "\t" << val[4]
                  << std::endl;
        testSum["results"]["HVV"][index] = hv_v;
        testSum["results"]["HVI"][index] = hv_i;
        testSum["results"]["AMACGAIN0"][index] = val[0];
        testSum["results"]["AMACGAIN1"][index] = val[1];
        testSum["results"]["AMACGAIN2"][index] = val[2];
        testSum["results"]["AMACGAIN4"][index] = val[3];
        testSum["results"]["AMACGAIN8"][index] = val[4];
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

json measureLvIV(std::shared_ptr<PowerSupplyChannel> lv) {
    logger(logINFO) << "## Measure LV IV  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "VINIIN";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    double v_start = 2.0;
    double v_end = 6.0;
    double v_step = 0.1;
    logger(logINFO) << " --> Running from " << v_start << "V to " << v_end
                    << "V in steps of " << v_step << "V";
    lv->turnOff();
    lv->setVoltageLevel(v_start);
    lv->setCurrentLevel(1.0);
    lv->turnOn();

    std::cout << "Vset\tVread\tIread" << std::endl;
    int index = 0;
    for (double vset = v_start; vset <= v_end; vset += v_step) {
        lv->setVoltageLevel(vset);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        double v_read = lv->measureVoltage();
        double i_read = lv->measureCurrent();

        std::cout << vset << "\t" << v_read << "\t" << i_read << std::endl;
        testSum["results"]["VINSET"][index] = vset;
        testSum["results"]["VIN"][index] = v_read;
        testSum["results"]["IIN"][index] = i_read;
        index++;
    }

    logger(logINFO) << " --> Done!";

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

json calibVinResponse(std::shared_ptr<AMACv2> amac,
                      std::shared_ptr<PowerSupplyChannel> lv) {
    logger(logINFO) << "## Measure LV IV  ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "VIN";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    double v_start = 6.0;
    double v_end = 11.0;
    double v_step = 0.1;
    logger(logINFO) << " --> Running from " << v_start << "V to " << v_end
                    << "V in steps of " << v_step << "V";

    std::cout << "VINSET\tVIN\tIIN\tAMACDCDCIN" << std::endl;
    int index = 0;
    for (double vset = v_start; vset <= v_end; vset += v_step) {
        lv->setVoltageLevel(vset);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        int DCDCin;
        try {
            DCDCin = amac->readAM(AMACv2::AM::DCDCIN);
        } catch (EndeavourComException &e) {
            logger(logERROR) << e.what();
            testSum["error"] = e.what();
            return testSum;
        }
        double v_read = lv->measureVoltage();
        double i_read = lv->measureCurrent();

        std::cout << vset << "\t" << v_read << "\t" << i_read << "\t" << DCDCin
                  << std::endl;
        testSum["results"]["VINSET"][index] = vset;
        testSum["results"]["VIN"][index] = v_read;
        testSum["results"]["IIN"][index] = i_read;
        testSum["results"]["AMACDCDCIN"][index] = DCDCin;
        index++;
    }

    lv->setVoltageLevel(11.0);

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    amac->initRegisters();

    return testSum;
}

json calibrateAMACCur10V(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                         uint32_t tests) {
    logger(logINFO) << "## Calibrating input CM block ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "CUR10V";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    uint32_t max_idcdc = pow(
        2,
        amac->getFieldWidth(
            &AMACv2RegMap::DCDCiP));  // maximum number for idcdcP/N to choose
    uint32_t max_iOffset =
        pow(2,
            amac->getFieldWidth(
                &AMACv2RegMap::DCDCiOffset));  // maximum number for
                                               // DCDCiOffset to choose

    bool skipOddiPiN = false;
    if (max_idcdc > 8)
        skipOddiPiN = true;  // too much scan for star, skip odd numbers

    // Store current state
    uint32_t DCDCen_curr = amac->rdField(&AMACv2RegMap::DCDCen);
    uint32_t DCDCenC_curr = amac->rdField(&AMACv2RegMap::DCDCenC);

    uint32_t DCDCiZeroReading_curr =
        amac->rdField(&AMACv2RegMap::DCDCiZeroReading);
    uint32_t DCDCiOffset_curr = amac->rdField(&AMACv2RegMap::DCDCiOffset);
    uint32_t DCDCiP_curr = amac->rdField(&AMACv2RegMap::DCDCiP);
    uint32_t DCDCiN_curr = amac->rdField(&AMACv2RegMap::DCDCiN);

    logger(logINFO) << " --> Turn off DCDC ...";
    amac->wrField(&AMACv2RegMap::DCDCen, 0);
    amac->wrField(&AMACv2RegMap::DCDCenC, 0);

    logger(logINFO) << " --> Starting measurement ...";

    uint32_t index = 0;
    uint32_t Cur10V, Cur10VTPL, Cur10VTPH;

    // Short the P/N
    for (uint32_t DCDCiZeroReading = 0; DCDCiZeroReading < 2;
         DCDCiZeroReading++) {
        amac->wrField(&AMACv2RegMap::DCDCiZeroReading, DCDCiZeroReading);
        logger(logDEBUG) << "Set DCDCiZeroReading = " << DCDCiZeroReading;
        for (uint32_t DCDCiOffset = 0; DCDCiOffset < max_iOffset;
             DCDCiOffset++) {
            amac->wrField(&AMACv2RegMap::DCDCiOffset, DCDCiOffset);
            logger(logDEBUG) << "Set DCDCiOffset = " << DCDCiOffset;
            for (uint32_t DCDCiP = 0; DCDCiP < max_idcdc; DCDCiP++) {
                if (skipOddiPiN && (DCDCiP % 2 == 1)) continue;
                amac->wrField(&AMACv2RegMap::DCDCiP, DCDCiP);
                logger(logDEBUG) << "Set DCDCiP = " << DCDCiP;
                for (uint32_t DCDCiN = 0; DCDCiN < max_idcdc; DCDCiN++) {
                    if (skipOddiPiN && (DCDCiN % 2 == 1)) continue;
                    amac->wrField(&AMACv2RegMap::DCDCiN, DCDCiN);
                    logger(logDEBUG) << "Set DCDCiN = " << DCDCiN;

                    for (uint32_t i = 0; i < tests; i++) {
                        Cur10V = amac->readAM(AMACv2::AM::CUR10V);
                        Cur10VTPL = amac->readAM(AMACv2::AM::CUR10VTPL);
                        Cur10VTPH = amac->readAM(AMACv2::AM::CUR10VTPH);

                        testSum["results"]["DCDCiZeroReading"][index] =
                            DCDCiZeroReading;
                        testSum["results"]["DCDCiOffset"][index] = DCDCiOffset;
                        testSum["results"]["DCDCiP"][index] = DCDCiP;
                        testSum["results"]["DCDCiN"][index] = DCDCiN;
                        testSum["results"]["AMACCUR10V"][index] = Cur10V;
                        testSum["results"]["AMACCUR10VTPL"][index] = Cur10VTPL;
                        testSum["results"]["AMACCUR10VTPH"][index] = Cur10VTPH;
                        index++;
                    }
                }
            }
        }
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCenC_curr);
    amac->wrField(&AMACv2RegMap::DCDCiZeroReading, DCDCiZeroReading_curr);
    amac->wrField(&AMACv2RegMap::DCDCiOffset, DCDCiOffset_curr);
    amac->wrField(&AMACv2RegMap::DCDCiP, DCDCiP_curr);
    amac->wrField(&AMACv2RegMap::DCDCiN, DCDCiN_curr);

    return testSum;
}

json calibrateAMACCur1V(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                        uint32_t tests) {
    logger(logINFO) << "## Calibrating output CM block ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "CUR1V";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    tb->setLoad(pbNum, 0.);
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    uint32_t max_odcdc = pow(
        2,
        amac->getFieldWidth(
            &AMACv2RegMap::DCDCoP));  // maximum number for odcdcP/N to choose
    uint32_t max_oOffset =
        pow(2,
            amac->getFieldWidth(
                &AMACv2RegMap::DCDCoOffset));  // maximum number for
                                               // DCDCoOffset to choose
    // Store current state
    uint32_t DCDCen_curr = amac->rdField(&AMACv2RegMap::DCDCen);
    uint32_t DCDCenC_curr = amac->rdField(&AMACv2RegMap::DCDCenC);

    uint32_t DCDCoZeroReading_curr =
        amac->rdField(&AMACv2RegMap::DCDCoZeroReading);
    uint32_t DCDCoOffset_curr = amac->rdField(&AMACv2RegMap::DCDCoOffset);
    uint32_t DCDCoP_curr = amac->rdField(&AMACv2RegMap::DCDCoP);
    uint32_t DCDCoN_curr = amac->rdField(&AMACv2RegMap::DCDCoN);

    logger(logINFO) << " --> Turn on DCDC ...";
    amac->wrField(&AMACv2RegMap::DCDCen, 1);
    amac->wrField(&AMACv2RegMap::DCDCenC, 1);

    logger(logINFO) << " --> Starting measurement ...";
    // Set sub-channel
    uint32_t index = 0;
    uint32_t Cur1V, Cur1VTPL, Cur1VTPH;

    for (uint32_t DCDCoZeroReading = 0; DCDCoZeroReading < 2;
         DCDCoZeroReading++) {
        amac->wrField(&AMACv2RegMap::DCDCoZeroReading, DCDCoZeroReading);
        logger(logDEBUG) << "Set DCDCoZeroReading = " << DCDCoZeroReading;
        for (uint32_t DCDCoOffset = 0; DCDCoOffset < max_oOffset;
             DCDCoOffset++) {
            amac->wrField(&AMACv2RegMap::DCDCoOffset, DCDCoOffset);
            logger(logDEBUG) << "Set DCDCoOffset = " << DCDCoOffset;
            for (uint32_t DCDCoP = 0; DCDCoP < max_odcdc; DCDCoP++) {
                amac->wrField(&AMACv2RegMap::DCDCoP, DCDCoP);
                logger(logDEBUG) << "Set DCDCoP = " << DCDCoP;
                for (uint32_t DCDCoN = 0; DCDCoN < max_odcdc; DCDCoN++) {
                    amac->wrField(&AMACv2RegMap::DCDCoN, DCDCoN);
                    logger(logDEBUG) << "Set DCDCoN = " << DCDCoN;

                    for (uint32_t i = 0; i < tests; i++) {
                        Cur1V = amac->readAM(AMACv2::AM::CUR1V);
                        Cur1VTPL = amac->readAM(AMACv2::AM::CUR1VTPL);
                        Cur1VTPH = amac->readAM(AMACv2::AM::CUR1VTPH);

                        testSum["results"]["DCDCoZeroReading"][index] =
                            DCDCoZeroReading;
                        testSum["results"]["DCDCoOffset"][index] = DCDCoOffset;
                        testSum["results"]["DCDCoP"][index] = DCDCoP;
                        testSum["results"]["DCDCoN"][index] = DCDCoN;
                        testSum["results"]["AMACCUR1V"][index] = Cur1V;
                        testSum["results"]["AMACCUR1VTPL"][index] = Cur1VTPL;
                        testSum["results"]["AMACCUR1VTPH"][index] = Cur1VTPH;
                        index++;
                    }
                }
            }
        }
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::DCDCen, DCDCen_curr);
    amac->wrField(&AMACv2RegMap::DCDCenC, DCDCenC_curr);
    amac->wrField(&AMACv2RegMap::DCDCoZeroReading, DCDCoZeroReading_curr);
    amac->wrField(&AMACv2RegMap::DCDCoOffset, DCDCoOffset_curr);
    amac->wrField(&AMACv2RegMap::DCDCoP, DCDCoP_curr);
    amac->wrField(&AMACv2RegMap::DCDCoN, DCDCoN_curr);

    return testSum;
}

json toggleOutput(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, bool hbipc) {
    logger(logINFO) << "## Test TOGGLEOUTPUT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "TOGGLEOUTPUT";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::cout << "\t\t\t"
              << "Off"
              << "\t\t"
              << "On" << std::endl;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    uint32_t ShuntEn_curr = amac->rdField(&AMACv2::ShuntEn);
    amac->wrField(&AMACv2::ShuntEn, 1);
    if (!hbipc) {
        PBv3Utils::mergeResult(
            testSum, PBv3TestTools::toggleOutputHelper(
                         pbNum, tb, PBv3TB::OFout, 1, 0, -0.01, 0.01, 1.0, 1.5,
                         "OFout", &AMACv2::RstCntOF, &AMACv2::RstCntCOF));
    }
    PBv3Utils::mergeResult(
        testSum, PBv3TestTools::toggleOutputHelper(
                     pbNum, tb, PBv3TB::SHUNTx, 0xFF, 0x00, 0.10, 0.30, 0.95,
                     1.2, "Shuntx", &AMACv2::DACShuntx));
    PBv3Utils::mergeResult(
        testSum, PBv3TestTools::toggleOutputHelper(
                     pbNum, tb, PBv3TB::SHUNTy, 0xFF, 0x00, 0.10, 0.30, 0.95,
                     1.2, "Shunty", &AMACv2::DACShunty));
    PBv3Utils::mergeResult(
        testSum, PBv3TestTools::toggleOutputHelper(
                     pbNum, tb, PBv3TB::CALx, 0xFF, 0x00, -0.10, 0.10, 0.75,
                     1.0, "CALx", &AMACv2::DACCalx));
    PBv3Utils::mergeResult(
        testSum, PBv3TestTools::toggleOutputHelper(
                     pbNum, tb, PBv3TB::CALy, 0xFF, 0x00, -0.10, 0.10, 0.75,
                     1.0, "CALy", &AMACv2::DACCaly));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDx0EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDx0EN",
            &AMACv2::CntSetHxLDO0en, &AMACv2::CntSetCHxLDO0en));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDx1EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDx1EN",
            &AMACv2::CntSetHxLDO1en, &AMACv2::CntSetCHxLDO1en));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDx2EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDx2EN",
            &AMACv2::CntSetHxLDO2en, &AMACv2::CntSetCHxLDO2en));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDy0EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDy0EN",
            &AMACv2::CntSetHyLDO0en, &AMACv2::CntSetCHyLDO0en));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDy1EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDy1EN",
            &AMACv2::CntSetHyLDO1en, &AMACv2::CntSetCHyLDO1en));
    PBv3Utils::mergeResult(
        testSum,
        PBv3TestTools::toggleOutputHelper(
            pbNum, tb, PBv3TB::LDy2EN, 0, 1, -0.01, 0.01, 1.0, 1.5, "LDy2EN",
            &AMACv2::CntSetHyLDO2en, &AMACv2::CntSetCHyLDO2en));

    logger(logINFO) << "## End test TOGGLEOUTPUT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2::ShuntEn, ShuntEn_curr);

    return testSum;
}

json toggleOutputHelper(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                        PBv3TB::CARRIER_OUTPUT muxCh, uint32_t on, uint32_t off,
                        double offLowThresh, double offHighThresh,
                        double onLowThresh, double onHighThresh,
                        const std::string &outString,
                        AMACv2Field AMACv2RegMap::*amacCh,
                        AMACv2Field AMACv2RegMap::*amacChCopy) {
    double offValue = 0;
    double onValue = 0;
    json testSum;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    // Determine original register state
    uint32_t orig_state = amac->rdField(amacCh);
    uint32_t orig_state_copy = (amacChCopy) ? (amac->rdField(amacChCopy)) : 0;

    // Toggle off
    amac->wrField(amacCh, off);
    if (amacChCopy) amac->wrField(amacChCopy, off);
    offValue = tb->readCarrierOutput(pbNum, muxCh);

    // Toggle on
    amac->wrField(amacCh, on);
    if (amacChCopy) amac->wrField(amacChCopy, on);
    onValue = tb->readCarrierOutput(pbNum, muxCh);

    // Revert to original state
    amac->wrField(amacCh, orig_state);
    if (amacChCopy) amac->wrField(amacChCopy, orig_state_copy);

    if (offValue < offHighThresh && offValue > offLowThresh &&
        onValue > onLowThresh && onValue < onHighThresh) {
        logger(logINFO) << outString << ": "
                        << "\t" << offValue << "\t\t" << onValue;
        testSum["passed"] = true;
    } else {
        logger(logERROR) << outString << ": "
                         << "\t" << offValue << "\t\t" << onValue;
        testSum["passed"] = false;
    }

    testSum["results"][outString + "_reg_value"][0] = off;
    testSum["results"][outString + "_value"][0] = offValue;
    testSum["results"][outString + "_reg_value"][1] = on;
    testSum["results"][outString + "_value"][1] = onValue;

    return testSum;
}

json rampDAC(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
             const std::string &dacName, PBv3TB::CARRIER_OUTPUT adcCh,
             AMACv2::AM am) {
    logger(logINFO) << "## Test RAMP" << dacName << " ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "RAMP" + dacName;
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    const std::string &amstr = "AMAC" + AMACv2::Map_AMStr.at(am);

    // Determine original register state
    uint32_t dac_curr = amac->rdField(dacName);
    uint32_t ShuntEn_curr = amac->rdField(&AMACv2::ShuntEn);
    amac->wrField(&AMACv2::ShuntEn, 1);

    // Ramp the DAC
    std::cout << "DAC"
              << "\t"
              << "External"
              << "\t"
              << "Internal" << std::endl;
    for (uint32_t dac = 0; dac <= 0xFF; dac++) {
        // Set value
        if (dac % 2 == 1) continue;  // skip odd to save half of time
        amac->wrField(dacName, dac);
        float output = tb->readCarrierOutput(pbNum, adcCh);
        uint32_t amval = amac->readAM(am);

        std::cout << dac << "\t" << output << "\t" << amval << std::endl;

        testSum["results"][dacName][dac] = dac;
        testSum["results"]["value"][dac] = output;
        testSum["results"][amstr][dac] = amval;
    }

    // Revert to old state
    amac->wrField(dacName, dac_curr);
    amac->wrField(&AMACv2::ShuntEn, ShuntEn_curr);

    logger(logINFO) << "## End test RAMP" << dacName << " ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

json scanPADID(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, bool hbipc) {
    logger(logINFO) << "## Test PADID ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "PADID";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    // Power cycle AMAC if the ID is already set
    if (amac->isCommIDSet()) {
        tb->setOFin(pbNum, true);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        tb->setOFin(pbNum, false);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }

    for (uint32_t i = 0; i <= 16; i++) {
        try {
            amac->EndeavourCom::setid(EndeavourCom::REFMODE::IDPads, i);
            testSum["results"]["PADID"] = i;
            testSum["results"]["eFuse"] = amac->readEFuse();
            if (i == 0 || hbipc) {
                logger(logINFO) << " --> PADID: " << i;
                testSum["passed"] = true;
            } else {
                logger(logERROR) << " --> PADID: " << i;
            }
            break;
        } catch (const EndeavourComException &e) {
            testSum["passed"] = false;
            if (i == 16) {
                logger(logERROR) << "Could not determine PADID";
            }
        }
    }

    // Power cycle AMAC to allow for resetting of the ID
    if (!hbipc) {
        tb->setOFin(pbNum, true);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        tb->setOFin(pbNum, false);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    if (testSum["passed"])
        logger(logINFO) << " PADID passed! :)";
    else
        logger(logERROR) << " PADID failed! :(";

    logger(logINFO) << "## End test PADID ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json temperature(std::shared_ptr<AMACv2> amac) {
    logger(logINFO) << "## Test TEMPERATURE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    ;
    json testSum;
    testSum["testType"] = "TEMPERATURE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    bool is_cold_test = false;
    if (amac->temperaturePB() < 0.0)
        is_cold_test = true;  // nominal cold test temperature: -30C

    uint32_t CTAToffset_curr = amac->rdField(&AMACv2RegMap::CTAToffset);
    uint32_t NTCx0SenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCx0SenseRange);
    uint32_t NTCy0SenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCy0SenseRange);
    uint32_t NTCpbSenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCpbSenseRange);

    amac->wrField(&AMACv2::CTAToffset, is_cold_test ? 8 : 4);
    amac->wrField(&AMACv2::NTCx0SenseRange, is_cold_test ? 7 : 4);
    amac->wrField(&AMACv2::NTCy0SenseRange, is_cold_test ? 7 : 4);
    amac->wrField(&AMACv2::NTCpbSenseRange, is_cold_test ? 0 : 7);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    uint32_t CTAT = amac->readAM(AMACv2::AM::CTAT);
    uint32_t NTCx = amac->readAM(AMACv2::AM::NTCX);
    uint32_t NTCy = amac->readAM(AMACv2::AM::NTCY);
    uint32_t NTCpb = amac->readAM(AMACv2::AM::NTCPB);
    uint32_t PTAT = amac->readAM(AMACv2::AM::PTAT);

    testSum["results"]["CTAToffset"] = is_cold_test ? 8 : 4;
    testSum["results"]["NTCx0SenseRange"] = is_cold_test ? 7 : 4;
    testSum["results"]["NTCy0SenseRange"] = is_cold_test ? 7 : 4;
    testSum["results"]["NTCpbSenseRange"] = is_cold_test ? 0 : 7;

    testSum["results"]["AMACCTAT"] = CTAT;
    testSum["results"]["AMACNTCX"] = NTCx;
    testSum["results"]["AMACNTCY"] = NTCy;
    testSum["results"]["AMACNTCPB"] = NTCpb;
    testSum["results"]["AMACPTAT"] = PTAT;

    uint32_t CTAT_low = is_cold_test ? 200 : 200;
    uint32_t CTAT_high = is_cold_test ? 600 : 600;
    uint32_t NTCx_low = is_cold_test ? 550 : 600;
    uint32_t NTCx_high = is_cold_test ? 850 : 850;
    uint32_t NTCy_low = is_cold_test ? 550 : 600;
    uint32_t NTCy_high = is_cold_test ? 850 : 850;
    uint32_t NTCpb_low = is_cold_test ? 500 : 600;
    uint32_t NTCpb_high = is_cold_test ? 1000 : 1000;
    uint32_t PTAT_low = is_cold_test ? 500 : 500;
    uint32_t PTAT_high = is_cold_test ? 900 : 900;

    // Pass fail checks
    if (CTAT_low < CTAT && CTAT < CTAT_high) {
        logger(logINFO) << " --> CTAT:\t" << CTAT;
    } else {
        logger(logERROR) << " --> CTAT:\t" << CTAT << " # not in [" << CTAT_low
                         << "," << CTAT_high << "]";
        testSum["passed"] = false;
    }

    if (NTCx_low < NTCx && NTCx < NTCx_high) {
        logger(logINFO) << " --> NTCx:\t" << NTCx;
    } else {
        logger(logERROR) << " --> NTCx:\t" << NTCx << " # not in [" << NTCx_low
                         << "," << NTCx_high << "]";
        testSum["passed"] = false;
    }

    if (NTCy_low < NTCy && NTCy < NTCy_high) {
        logger(logINFO) << " --> NTCy:\t" << NTCy;
    } else {
        logger(logERROR) << " --> NTCy:\t" << NTCy << " # not in [" << NTCy_low
                         << "," << NTCy_high << "]";
        testSum["passed"] = false;
    }

    if (NTCpb_low < NTCpb && NTCpb < NTCpb_high) {
        logger(logINFO) << " --> NTCpb:\t" << NTCpb;
    } else {
        logger(logERROR) << " --> NTCpb:\t" << NTCpb << " # not in ["
                         << NTCpb_low << "," << NTCpb_high << "]";
        testSum["passed"] = false;
    }

    // The (200,500) range is for compatibility with FEAST and should
    // be removed in the future.
    if ((200 < PTAT && PTAT < 500) || (PTAT_low < PTAT && PTAT < PTAT_high)) {
        logger(logINFO) << " --> PTAT:\t" << PTAT;
    } else {
        logger(logERROR) << " --> PTAT:\t" << PTAT
                         << " # not in [200, 500] or [" << PTAT_low << ","
                         << PTAT_high << "]";
        testSum["passed"] = false;
    }

    if (testSum["passed"])
        logger(logINFO) << " TEMPERATURE passed! :)";
    else
        logger(logERROR) << " TEMPERATURE failed! :(";

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::CTAToffset, CTAToffset_curr);
    amac->wrField(&AMACv2RegMap::NTCx0SenseRange, NTCx0SenseRange_curr);
    amac->wrField(&AMACv2RegMap::NTCy0SenseRange, NTCy0SenseRange_curr);
    amac->wrField(&AMACv2RegMap::NTCpbSenseRange, NTCpbSenseRange_curr);

    logger(logINFO) << "## End test TEMPERATURE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json temperatureNTC(std::shared_ptr<AMACv2> amac) {
    logger(logINFO) << "## Test TEMPERATURE_NTC ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    ;
    json testSum;
    testSum["testType"] = "TEMPERATURE_NTC";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    uint32_t NTCx0SenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCx0SenseRange);
    uint32_t NTCy0SenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCy0SenseRange);
    uint32_t NTCpbSenseRange_curr =
        amac->rdField(&AMACv2RegMap::NTCpbSenseRange);

    std::cout << "NTCx0SenseRange"
              << "\t"
              << "NTCy0SenseRange"
              << "\t"
              << "NTCpbSenseRange"
              << "\t"
              << "AMACNTCX"
              << "\t"
              << "AMACNTCY"
              << "\t"
              << "AMACNTCPB" << std::endl;

    uint32_t index = 0;
    for (uint32_t NTCSenseRange = 0; NTCSenseRange < 0x8; NTCSenseRange++) {
        amac->wrField(&AMACv2::NTCx0SenseRange, NTCSenseRange);
        amac->wrField(&AMACv2::NTCy0SenseRange, NTCSenseRange);
        amac->wrField(&AMACv2::NTCpbSenseRange, NTCSenseRange);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        uint32_t AMACNTCX = amac->readAM(AMACv2::AM::NTCX);
        uint32_t AMACNTCY = amac->readAM(AMACv2::AM::NTCY);
        uint32_t AMACNTCPB = amac->readAM(AMACv2::AM::NTCPB);

        std::cout << NTCSenseRange << "\t" << NTCSenseRange << "\t"
                  << NTCSenseRange << "\t" << AMACNTCX << "\t" << AMACNTCY
                  << "\t" << AMACNTCPB << std::endl;

        testSum["results"]["NTCx0SenseRange"][index] = NTCSenseRange;
        testSum["results"]["NTCy0SenseRange"][index] = NTCSenseRange;
        testSum["results"]["NTCpbSenseRange"][index] = NTCSenseRange;

        testSum["results"]["AMACNTCX"][index] = AMACNTCX;
        testSum["results"]["AMACNTCY"][index] = AMACNTCY;
        testSum["results"]["AMACNTCPB"][index] = AMACNTCPB;

        index++;
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::NTCx0SenseRange, NTCx0SenseRange_curr);
    amac->wrField(&AMACv2RegMap::NTCy0SenseRange, NTCy0SenseRange_curr);
    amac->wrField(&AMACv2RegMap::NTCpbSenseRange, NTCpbSenseRange_curr);

    logger(logINFO) << "## End test TEMPERATURE_NTC ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json temperatureCTAT(std::shared_ptr<AMACv2> amac) {
    logger(logINFO) << "## Test TEMPERATURE_CTAT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    ;
    json testSum;
    testSum["testType"] = "TEMPERATURE_CTAT";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    uint32_t CTAToffset_curr = amac->rdField(&AMACv2RegMap::CTAToffset);

    std::cout << "CTAToffset"
              << "\t"
              << "AMACCTAT" << std::endl;

    uint32_t index = 0;
    for (uint32_t CTAToffset = 0; CTAToffset <= 0xF; CTAToffset++) {
        amac->wrField(&AMACv2::CTAToffset, CTAToffset);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        uint32_t AMACCTAT = amac->readAM(AMACv2::AM::CTAT);

        std::cout << CTAToffset << "\t" << AMACCTAT << std::endl;

        testSum["results"]["CTAToffset"][index] = CTAToffset;

        testSum["results"]["AMACCTAT"][index] = AMACCTAT;

        index++;
    }

    testSum["results"]["TIMEEND"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Revert to old state
    amac->wrField(&AMACv2RegMap::CTAToffset, CTAToffset_curr);

    logger(logINFO) << "## End test TEMPERATURE_CTAT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

}  // namespace PBv3TestTools
