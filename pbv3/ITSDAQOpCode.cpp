#include "ITSDAQOpCode.h"

ITSDAQOpCode::ITSDAQOpCode(uint16_t seqNum, uint16_t opcode,
                           const std::vector<uint16_t> &payload)
    : m_seqNum(seqNum), m_opcode(opcode), m_payload(payload) {}

uint16_t ITSDAQOpCode::opcode() const { return m_opcode; }

std::vector<uint16_t> ITSDAQOpCode::payload() const { return m_payload; }

std::vector<uint16_t> ITSDAQOpCode::data() const {
    std::vector<uint16_t> rawdata(3 + m_payload.size());
    rawdata[0] = m_opcode;
    rawdata[1] = m_seqNum;
    rawdata[2] = m_payload.size() * 2;
    std::copy(m_payload.begin(), m_payload.end(), rawdata.begin() + 3);
    return rawdata;
}
