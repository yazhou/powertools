#include "PBv3TBConf.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include "PBv3TBRegistry.h"

////////////////////
// Configuration
////////////////////

PBv3TBConf::PBv3TBConf() {}

PBv3TBConf::~PBv3TBConf() {}

PBv3TBConf::PBv3TBConf(const std::string &hardwareConfigFile) {
    setHardwareConfig(hardwareConfigFile);
}

PBv3TBConf::PBv3TBConf(const json &hardwareConfig) {
    setHardwareConfig(hardwareConfig);
}

void PBv3TBConf::setHardwareConfig(const std::string &hardwareConfigFile) {
    // load JSON object from file
    std::ifstream i(hardwareConfigFile);
    if (i.is_open())
        i >> m_hardwareConfig;
    else {
        logger(logERROR) << "Unable to open hardware config: "
                         << hardwareConfigFile;
    }

    m_hw.setHardwareConfig(m_hardwareConfig);
}

void PBv3TBConf::setHardwareConfig(const json &hardwareConfig) {
    // store JSON config file
    m_hardwareConfig = hardwareConfig;

    m_hw.setHardwareConfig(m_hardwareConfig);
}

json PBv3TBConf::getPBv3TBConf(const std::string &label) {
    for (const auto &hw : m_hardwareConfig["testbenches"].items()) {
        // check label
        if (hw.key() == label) return hw.value();
    }
    return json();
}

////////////////////
// General private
////////////////////

std::shared_ptr<PBv3TB> PBv3TBConf::getPBv3TB(const std::string &label) {
    // first check if an object with the same label/type is already available
    // (was already instantiated)
    if (m_listPBv3TB.find(label) != m_listPBv3TB.end())
        return m_listPBv3TB[label];

    // Otherwise, create the object
    // check first if hardware configuration is available
    json reqConf = getPBv3TBConf(label);
    if (reqConf.empty()) {
        std::cerr << "Requested device not found in input configuration file: "
                  << label << std::endl;
        return nullptr;
    }

    // setup device
    std::shared_ptr<PBv3TB> tb = PBv3TBRegistry::createPBv3TB(reqConf["type"]);

    // configure the testbench
    tb->setConfiguration(reqConf);

    // get low voltage
    std::shared_ptr<PowerSupplyChannel> lv = m_hw.getPowerSupplyChannel("Vin");
    if (lv == nullptr) logger(logWARNING) << "No LV power supply available!";
    tb->setLVPS(lv);

    // get high voltage
    std::shared_ptr<PowerSupplyChannel> hv = m_hw.getPowerSupplyChannel("HVin");
    if (hv == nullptr) logger(logWARNING) << "No HV power supply available!";
    tb->setHVPS(hv);

    // initialize it
    tb->init();

    return tb;
}
