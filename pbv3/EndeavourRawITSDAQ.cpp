#include "EndeavourRawITSDAQ.h"

#include <iomanip>
#include <iostream>

#include "EndeavourComException.h"
#include "NotSupportedException.h"

// More info on the ITSDAQ op-codes
// https://twiki.cern.ch/twiki/bin/view/Atlas/ITSDAQDataFormat#EMORSE

#define CMD_SETID 0b110
#define CMD_WRITE 0b111
#define CMD_READ 0b101

EndeavourRawITSDAQ::EndeavourRawITSDAQ(std::shared_ptr<ITSDAQCom> com)
    : m_com(com) {}

EndeavourRawITSDAQ::~EndeavourRawITSDAQ() {}

void EndeavourRawITSDAQ::setDitMin(uint32_t DIT_MIN) {
    throw NotSupportedException("Variable length dits not supported.");
}

uint32_t EndeavourRawITSDAQ::getDitMin() {
    throw NotSupportedException("Variable length dits not supported.");
}

void EndeavourRawITSDAQ::setDitMid(uint32_t DIT_MID) {
    throw NotSupportedException("Variable length dits not supported.");
}

uint32_t EndeavourRawITSDAQ::getDitMid() {
    throw NotSupportedException("Variable length dits not supported.");
}

void EndeavourRawITSDAQ::setDitMax(uint32_t DIT_MAX) {
    throw NotSupportedException("Variable length dits not supported.");
}

uint32_t EndeavourRawITSDAQ::getDitMax() {
    throw NotSupportedException("Variable length dits not supported.");
}

void EndeavourRawITSDAQ::setDahMin(uint32_t DAH_MIN) {
    throw NotSupportedException("Variable length dahs not supported.");
}

uint32_t EndeavourRawITSDAQ::getDahMin() {
    throw NotSupportedException("Variable length dahs not supported.");
}

void EndeavourRawITSDAQ::setDahMid(uint32_t DAH_MID) {
    throw NotSupportedException("Variable length dahs not supported.");
}

uint32_t EndeavourRawITSDAQ::getDahMid() {
    throw NotSupportedException("Variable length dahs not supported.");
}

void EndeavourRawITSDAQ::setDahMax(uint32_t DAH_MAX) {
    throw NotSupportedException("Variable length dahs not supported.");
}

uint32_t EndeavourRawITSDAQ::getDahMax() {
    throw NotSupportedException("Variable length dahs not supported.");
}

void EndeavourRawITSDAQ::setBitGapMin(uint32_t BITGAP_MIN) {
    throw NotSupportedException("Variable length bit gap not supported.");
}

uint32_t EndeavourRawITSDAQ::getBitGapMin() {
    throw NotSupportedException("Variable length bit gap not supported.");
}

void EndeavourRawITSDAQ::setBitGapMid(uint32_t BITGAP_MID) {
    throw NotSupportedException("Variable length bit gap not supported.");
}

uint32_t EndeavourRawITSDAQ::getBitGapMid() {
    throw NotSupportedException("Variable length bit gap not supported.");
}

void EndeavourRawITSDAQ::setBitGapMax(uint32_t BITGAP_MAX) {
    throw NotSupportedException("Variable length bit gap not supported.");
}

uint32_t EndeavourRawITSDAQ::getBitGapMax() {
    throw NotSupportedException("Variable length bit gap not supported.");
}

void EndeavourRawITSDAQ::reset() {}

bool EndeavourRawITSDAQ::isError() { return m_error; }

bool EndeavourRawITSDAQ::isDataValid() { return m_valid; }

void EndeavourRawITSDAQ::sendData(unsigned long long int data,
                                  unsigned int size) {
    m_valid = false;
    m_error = false;

    // Determine command type (◔_◔)
    uint8_t cmd = (data >> (size - 3)) & 0x7;
    if (cmd != CMD_SETID && cmd != CMD_WRITE && cmd != CMD_READ)
        throw EndeavourComException("Trying to send an unknown command: %02x",
                                    cmd);

    //
    // Build network packet
    //
    // Send data
    std::vector<uint16_t> inBuffer(7);

    // header
    uint16_t chan = 0;         // TODO unhardcode
    inBuffer[0] = 0x0000;      // version
    inBuffer[1] = chan & 0xf;  // control
    inBuffer[2] = 0x0000;      // spare

    // packet data
    uint64_t dataBlock = data | (((uint64_t)(cmd == CMD_READ)) << 56);
    inBuffer[3] = ((dataBlock >> 48) & 0xFFFF);
    inBuffer[4] = ((dataBlock >> 32) & 0xFFFF);
    inBuffer[5] = ((dataBlock >> 16) & 0xFFFF);
    inBuffer[6] = ((dataBlock >> 0) & 0xFFFF);

    ITSDAQPacket packet(0, {ITSDAQOpCode(0, ITSDAQ_OPCODE_EMORSE, inBuffer)});
    m_com->send(packet);

    //
    // Receive data
    packet = m_com->receive();

    if (packet.nOpcodes() == 0)
        throw EndeavourComException("ITSDAQ returned no data");

    // Parse result
    ITSDAQOpCode opcode = packet.opcode(0);
    std::vector<uint16_t> payload = opcode.payload();

    //
    // Reconstruct the AMAC response
    uint64_t code = (payload[3] >> 12) & 0x0F;  // timeout or error
    uint64_t amac = (payload[4] >> 11) & 0x1F;
    uint64_t addr = (payload[4] >> 0) & 0xFF;
    uint64_t regv = (((uint64_t)payload[5]) << 16 | payload[6]);

    if (code == 0xF)  // Timeout!
    {
        m_valid = false;
        return;
    }

    switch (cmd) {
        case CMD_SETID: {
            uint64_t retid = (payload[6] >> 3) & 0x1F;
            m_readData = (retid << 3) | (((uint64_t)0) << 0);
            m_readSize = 8;
            m_valid = true;
            m_error = false;
        } break;
        case CMD_WRITE: {
            // ITSDAQ firmware does not return AMAC ID
            amac = (((data >> 8) >> 32) >> 8) & 0x1F;
            m_readData = (amac << 3) | (((uint64_t)0) << 0);
            m_readSize = 8;
            m_valid = true;
            m_error = false;
        } break;
        case CMD_READ: {
            m_readData = (amac << 43) | (((uint64_t)0) << 40) | (addr << 32) |
                         (regv << 0);
            m_readSize = 48;
            m_valid = true;
            m_error = false;
        } break;
        default: {
            m_valid = false;
            m_error = true;
        } break;
    }
}

void EndeavourRawITSDAQ::readData(unsigned long long int &data,
                                  unsigned int &size) {
    data = m_readData;
    size = m_readSize;
}
