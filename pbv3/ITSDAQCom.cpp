#include "ITSDAQCom.h"

#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h> /* See NOTES */
#include <unistd.h>

#include <cstring>
#include <iomanip>
#include <iostream>

#include "ITSDAQComException.h"

ITSDAQCom::ITSDAQCom(const std::string &addr, uint32_t port) {
    // socket creation
    m_socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (m_socket < 0) throw ITSDAQComException("socket creation failed");

    // sending data
    memset(&m_fpgaaddr, 0, sizeof(m_fpgaaddr));
    m_fpgaaddr.sin_family = AF_INET;
    m_fpgaaddr.sin_port = htons(port);
    m_fpgaaddr.sin_addr.s_addr = inet_addr(addr.c_str());

    // receiveing responses
    struct sockaddr_in myaddr;
    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(port);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(m_socket, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0)
        throw ITSDAQComException("failed to bind UDP socket");

    int32_t enable = 1;
    setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int32_t));
}

ITSDAQCom::~ITSDAQCom() {
    if (m_socket > 0) close(m_socket);
}

void ITSDAQCom::send(const ITSDAQPacket &packet) {
    const std::vector<uint16_t> &prawdata = packet.rawdata();

    std::vector<uint8_t> rawdata(prawdata.size() * 2);
    for (uint32_t i = 0; i < prawdata.size(); i++) {
        rawdata[2 * i + 0] = ((prawdata[i] >> 8) & 0xFF);
        rawdata[2 * i + 1] = ((prawdata[i] >> 0) & 0xFF);
    }

    /*
    std::cout << "start send" << std::endl;
    for(uint8_t x : rawdata)
      std::cout << "0x" << std::hex << std::setw(2) << std::setfill('0') <<
    (uint32_t)x << std::dec << std::endl; std::cout << "end send" << std::endl;
    */

    ssize_t ssize =
        ::sendto(m_socket, &rawdata[0], rawdata.size(), 0,
                 (struct sockaddr *)&m_fpgaaddr, sizeof(m_fpgaaddr));
    if (ssize < 0)
        throw ITSDAQComException("send error: %s", std::strerror(errno));
}

ITSDAQPacket ITSDAQCom::receive() {
    // Wait for response
    fd_set selList;
    FD_ZERO(&selList);
    FD_SET(m_socket, &selList);

    timeval timeOutVal;
    timeOutVal.tv_sec = 0;
    timeOutVal.tv_usec = 1000 * 100;  // 100 ms

    int32_t result = select(m_socket + 1, &selList, NULL, NULL, &timeOutVal);
    if (result != 1)
        throw ITSDAQComException("No or too many replies from FPGA: " +
                                 std::to_string(result));

    std::vector<uint8_t> buffer(128);
    ssize_t rsize = ::recv(m_socket, &buffer[0], 128, 0);
    if (rsize < 0)
        throw ITSDAQComException("recv error: %s", std::strerror(errno));

    std::vector<uint16_t> rawdata(rsize / 2);
    for (uint32_t i = 0; i < rsize / 2; i++)
        rawdata[i] = (buffer[2 * i + 0] << 8) | (buffer[2 * i + 1] << 0);

    /*std::cout << "start receive" << std::endl;
    for(uint16_t x : rawdata)
      std::cout << "0x" << std::hex << std::setw(4) << std::setfill('0') <<
    (uint32_t)x << std::dec << std::endl; std::cout << "end receive" <<
    std::endl;*/

    return ITSDAQPacket(rawdata);
}
