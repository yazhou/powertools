#ifndef PBV3TBSINGLE_H
#define PBV3TBSINGLE_H

#include <memory>

#include "Bk85xx.h"
#include "Keithley24XX.h"
#include "PBv3TB.h"

/** \brief Testbench for controlling a Powerboard on a single-board carrier.
 *
 * It consists of the following hardware:
 * - LV/HV power supplies
 * - External load (Bk85xx)
 * - FTDI-based communication dongle
 */
class PBv3TBSingle : public PBv3TB {
 public:
    /**
     * Constructor
     */
    PBv3TBSingle() = default;

    virtual ~PBv3TBSingle() = default;

    /** \brief Configure on JSON object
     *
     * Valid keys:
     *  - `ftdi_description`: Serial number of the FT232H dongle (default: None)
     *  - `ftdi_serial`: Product description of the FT232H dongle (default:
     * None)
     *  - `bkdev`: Linux device for serial communication with the BK load
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json &config);

    /** \brief Initialize testbench
     *
     * Calls `initLoad and initPB`
     */
    virtual void init();

    std::shared_ptr<AMACv2> getPB(uint8_t pbNum);

    void setOFin(uint8_t pbNum, bool value);

    double getVin();

    double getNTC(uint8_t ntc);
    double getActiveTemp();
    double getActiveHum();
    double getActiveDew();

    double getIload(uint8_t pbNum);

    //! \brief Return a pointer to the load
    std::shared_ptr<Bk85xx> getLoadPtr();
    void loadOn(uint8_t pbNum);
    void loadOff(uint8_t pbNum);
    double setLoad(uint8_t pbNum, double load);
    double getLoad(uint8_t pbNum);
    double getVout(uint8_t pbNum);

    double getHVoutCurrent(uint8_t pbNum);

    double readCarrierOutput(uint32_t pbNum, PBv3TB::CARRIER_OUTPUT value);

 private:
    /*
     * \brief Initialize external load
     *
     * - Create object and initialize communication
     * - Disable remote sense
     */
    void initLoad();

    /*
     * \brief Initialize the PB communication object
     */
    void initPB();

 private:
    //! Serial number of the FT232H dongle to use
    std::string m_ftdi_serial;

    //! Product description of the FT232H dongle to use
    std::string m_ftdi_description;

    //! Linux device for serial communication with the BK load
    std::string m_bkdev;

    std::shared_ptr<Bk85xx> m_load = nullptr;
    std::shared_ptr<AMACv2> m_pb = nullptr;
};

#endif  // PBV3TBSINGLE_H
