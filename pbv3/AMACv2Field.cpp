#include "AMACv2Field.h"

AMACv2Field::AMACv2Field(const std::string &fieldName, AMACv2Register *reg,
                         uint8_t offset, uint8_t width, uint32_t defaultVal)
    : m_fieldName(fieldName),
      m_register(reg),
      m_offset(offset),
      m_width(width),
      m_defaultVal(defaultVal) {
    m_mask = (uint32_t)(((1 << m_width) - 1) << m_offset);
    if (reg->isRW() != RO) this->write(m_defaultVal);
}

bool AMACv2Field::isValid() const { return !m_fieldName.empty(); }

bool AMACv2Field::canBeWrittenField() const {
    if (!isValid()) return false;
    return (m_register->isRW() != RO);
}

bool AMACv2Field::canBeReadField() const {
    if (!isValid()) return false;
    return (m_register->isRW() != WO);
}

bool AMACv2Field::isReadWrite() const {
    if (!isValid()) return false;
    return (m_register->isRW() == RW);
}

std::string AMACv2Field::getName() const { return m_fieldName; }

AMACv2Register *AMACv2Field::getRegister() const { return m_register; }

uint8_t AMACv2Field::getWidth() const { return m_width; }

void AMACv2Field::setDefaultVal(uint32_t defaultVal) {
    m_defaultVal = defaultVal;
}

void AMACv2Field::writeDefaultVal() {
    if (!isValid()) return;
    if (m_register->isRW() == RO) {
        std::cerr << " --> Error: Read-only register \"" << m_fieldName << "\""
                  << std::endl;
        return;
    }
    write(m_defaultVal);
}

void AMACv2Field::write(const uint32_t &cfgBits) {
    if (!isValid()) return;
    if (m_register->isRW() == RO) {
        std::cerr << " --> Error: Read-only register \"" << m_fieldName << "\""
                  << std::endl;
        return;
    }
    uint32_t value = m_register->getValue();
    value = (value & ~m_mask) | ((cfgBits << m_offset) & m_mask);
    m_register->setValue(value);
}

uint32_t AMACv2Field::read() const {
    if (!isValid()) return 0;
    if (m_register->isRW() == WO) {
        std::cerr << " --> Error: Write-only register \"" << m_fieldName << "\""
                  << std::endl;
        return 0;
    }
    return ((m_register->getValue() & m_mask) >> m_offset);
}
