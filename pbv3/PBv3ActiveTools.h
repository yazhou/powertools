#ifndef PBV3ACTIVETOOLS_H
#define PBV3ACTIVETOOLS_H

#include <nlohmann/json.hpp>

#include "PBv3TBMassive.h"

/**
 * \brief Module defining several tests for an empty active board
 *
 *  - All tests return a JSON format that can be directly
 *    uploaded to the ITk Production Database
 */
namespace PBv3ActiveTools {

//! \brief LV_ENABLE Check current with 11V on
/**
 * Pass: Input current is below 5 mA.
 *
 * \param tb Testbench object
 *
 * \return json object with test results
 */
nlohmann::json testLvEnable(std::shared_ptr<PBv3TBMassive> tb);

//! \brief I2CDEV Check communication with all I2C devices on active board
/**
 * Communication is tested by performing a 0 byte write operation to
 * the I2C device. If it works, then no exception should be thrown.
 *
 * Pass: Communication with all devices ok
 *
 * \param tb Testbench object
 *
 * \return json object with test results
 */
nlohmann::json testI2C(std::shared_ptr<PBv3TBMassive> tb);

//! \brief PSINIT Check communication with powersupplies and turn them off
/**
 *
 * Pass: Voltage and current of HV and LV power supplies is low in OFF status
 *
 * \param tb Testbench object
 * \param skiphv whether or not to skip HV checks
 *
 * \return json object with test results
 */
nlohmann::json testPSINIT(std::shared_ptr<PBv3TBMassive> tb,
                          bool skiphv = false);

}  // namespace PBv3ActiveTools

#endif  // PBV3ACTIVETOOLS_H
