#ifndef PBV3TBMASSIVE20190718_H
#define PBV3TBMASSIVE20190718_H

#include "PBv3TBMassive.h"

class PBv3TBMassive20190718 : public PBv3TBMassive {
 public:
    PBv3TBMassive20190718(std::shared_ptr<PowerSupplyChannel> lv,
                          std::shared_ptr<PowerSupplyChannel> hv);
    PBv3TBMassive20190718(std::shared_ptr<EquipConf> hw);
    PBv3TBMassive20190718() = default;
    ~PBv3TBMassive20190718() = default;

 protected:
    //! Initialize devices on the active board
    void initDevices();
};

#endif  // PBV3TBMASSIVE20190718_H
