#include "PBv3TBModule.h"

#include <chrono>
#include <cmath>
#include <thread>

#include "AD56X9.h"
#include "EndeavourRawITSDAQ.h"
#include "ITSDAQI2CCom.h"
#include "Logger.h"
#include "OutOfRangeException.h"

// Register test bench
#include "PBv3TBRegistry.h"
REGISTER_PBV3TB(PBv3TBModule)

PBv3TBModule::PBv3TBModule(const std::string &ip, uint32_t port)
    : PBv3TB(), m_ip(ip), m_port(port) {}

PBv3TBModule::~PBv3TBModule() {}

void PBv3TBModule::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "ip") {
            m_ip = kv.value();
        } else if (kv.key() == "port") {
            m_port = kv.value();
        } else if (kv.key() == "pbs") {
            for (const auto &pb : kv.value()) {
                m_pb_idpads.push_back(pb.value("idpads", 0));
                m_pb_commid.push_back(pb.value("commid", 0));
            }
        }
    }

    PBv3TB::setConfiguration(config);
}

void PBv3TBModule::init() {
    // PB com
    m_com = std::make_shared<ITSDAQCom>(m_ip, m_port);
    for (uint32_t i = 0; i < m_pb_commid.size(); i++) {
        m_pbs.push_back(std::make_shared<AMACv2>(
            m_pb_commid[i], std::unique_ptr<EndeavourRawITSDAQ>(
                                new EndeavourRawITSDAQ(m_com))));
        m_pbs.back()->setPADID(m_pb_idpads[i]);
    }

    // CAL
    std::shared_ptr<I2CCom> i2c = std::make_shared<ITSDAQI2CCom>(0x56, m_com);
    setCalDAC(std::make_shared<AD56X9>(1.5, AD56X9::Model::AD5629, i2c));
}

void PBv3TBModule::setOFin(uint8_t pbNum, bool value) {
    logger(logWARNING) << "OFin not implemented on module yet.";
}

double PBv3TBModule::getVin() {
    logger(logWARNING) << "LV is monitored externally on the module.";
    return 0.;
}

double PBv3TBModule::getNTC(uint8_t ntc) {
    logger(logWARNING) << "getNTC is not implemented on module yet.";
    return 0.;
}

double PBv3TBModule::getActiveTemp() {
    logger(logWARNING) << "ActiveTemp is not implemented on module yet.";
    return 0.;
}

double PBv3TBModule::getActiveHum() {
    logger(logWARNING) << "ActiveHum is not implemented on module yet.";
    return 0.;
}

double PBv3TBModule::getActiveDew() {
    logger(logWARNING) << "ActiveDew is not implemented on module yet.";
    return 0.;
}

std::shared_ptr<AMACv2> PBv3TBModule::getPB(uint8_t pbNum) {
    if (pbNum >= m_pbs.size())
        throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);
    return m_pbs[pbNum];
}

void PBv3TBModule::loadOn(uint8_t pbNum) {
    logger(logWARNING) << "No load on module.";
}

void PBv3TBModule::loadOff(uint8_t pbNum) {
    logger(logWARNING) << "No load on module.";
}

double PBv3TBModule::setLoad(uint8_t pbNum, double load) {
    logger(logWARNING) << "No load on module.";
    return 0.;
}
double PBv3TBModule::getLoad(uint8_t pbNum) {
    logger(logWARNING) << "No load on module.";
    return 0.;
}

double PBv3TBModule::getVout(uint8_t pbNum) {
    logger(logWARNING) << "No DC/DC monitoring on module.";
    return 0.;
}

double PBv3TBModule::getIload(uint8_t pbNum) {
    logger(logWARNING) << "No load on module.";
    return 0.;
}

// Measure the high voltage
double PBv3TBModule::getHVout(uint8_t pbNum) {
    logger(logWARNING) << "HV is monitored externally on module.";
    return 0.;
}

void PBv3TBModule::powerHVOn() {
    logger(logWARNING) << "HV is controlled externally on module.";
}

void PBv3TBModule::powerHVOff() {
    logger(logWARNING) << "HV is controlled externally on module.";
}

double PBv3TBModule::getHVoutCurrent(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    return 0.;  // TODO implement
}

double PBv3TBModule::readCarrierOutput(uint32_t pbNum,
                                       PBv3TB::CARRIER_OUTPUT value) {
    logger(logWARNING)
        << "Powerboard output reading not setup for single testbench.";

    return 0.;
}
